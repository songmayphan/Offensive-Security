# NMAP
-Network Mapping - port scanning tool

## Usage
- Port scanning 
- Check firewalls, available ports

## Switches

- What is the first switch listed in the help menu for a 'Syn Scan' `-sS`
- Which switch would you use for a "UDP scan"? `-sU`
- If you wanted to detect which operating system the target is running on, which switch would you use?`-o`
- Nmap provides a switch to detect the version of the services running on the target. What is this switch?`-sV`
- The default output provided by nmap often does not provide enough information for a pentester. How would you increase the verbosity?`-v`
- Verbosity level one is good, but verbosity level two is better! How would you set the verbosity level to two?
(Note: it's highly advisable to always use at least this option)
`-vv`
- What switch would you use to save the nmap results in three major formats?
`-oA`
- What switch would you use to save the nmap results in a "normal" format?
`-oN`
- A very useful output format: how would you save results in a "grepable" format?
`-oG`
- shorthand switch that activates service detection, operating system detection, a traceroute and common script scanning.
`-a`
- How would you set the timing template to level 5? `-t5`
- How would you tell nmap to only scan port 80? `-p 80`
- How would you tell nmap to scan ports 1000-1500? `-p 1000-1500`
- How would you tell nmap to scan all ports?
`-p-`
- How would you activate a script from the nmap scripting library?
`--script`
- How would you activate all of the scripts in the "vuln" category?
`--script=vuln`


## SYN scan
- or Half-open, Stealth scan

1) Switch `-sS`, used to scan TCP port range of target
- Require `sudo`

2) Perform a TCP handshake with the target 
- If received a SYN/ACK  > port open
- If received a RST response > port close
- IF nothing received > firewall

