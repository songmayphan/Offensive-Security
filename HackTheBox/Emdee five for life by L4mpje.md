# MD5 encrypt 

- can't be done manually with `md5sum <<< "1bC59bDQYuBb4Tq6bLAf"`
- only way to be fast enough is to post a POST request as the page load


## Script

- python has Hashlib to encrypte MD5
- print the page with GET request

`req = requests.session()
url = <URL>
rget = req.get(url)
html = rget.content`
- Gotta encode it to utf-8 before hashing

`html = html.encode('utf-8)`

- remove all the uneccessary string

``` python
def clean_up(html):
    clean = re.compile('<.*?>')
    return re.sub(clean, '', html)
str_1 = clean_up(html)
str_2 = str_1.split('string')[1]
last = str_2.rstrip()

```



- Encrypt to MD5

`md5_encr = hashlib.md5(last).hexdigest()`

- Test with Burp proxy and found that the submit button carry a hash param

`data = dict(hash=md5_encr)`
`rpost = req.post(url=url, data=data)`

- Print to get flag

`print(rpost.text)`





