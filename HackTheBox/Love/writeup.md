# Metadata 
IP:  10.129.136.85
Date accessed: 2/6/2022

# Recon

## check http
1) go to IP in browser
 - See a login with the header "Voting System"
2) Bruteforce admin password
-`admin:admin`

- Looks like the first field is voter's ID and not username, maybe it's not admin
- it could be that admin login is in a different portal 
## Nmap 

`nmap -sV -sC -oN love-normalnmap 10.129.136.85 `

nmap results:

``


1) Adding "staging.love.htb" into our /etc/hosts file
`sudo nano /etc/hosts`
 add the link
 10.129.136.85   staging.love.htb

2) go to staging.love.htb
- found a webpage with a header `Free File Scanner`
3) In Demo tab, we see a field to insert URL 
4) Turn on BurpSuite and intercept
5) Inthe box, enter: `http://127.0.0.1:5000`
6) forward the message and got the admin pw
`Vote Admin Creds admin: @LoveIsInTheAir!!!!`

7) go to `http://10.129.136.85/admin/`
8) log in wi the new foudn credentials

