# Meta data 
IP: 
Date accessed: 2/14/2020

# Recon

- nmap: port 80 was open 
- add website to known hosts
- go to IP

## Search bar
- search bar input returns an error 
>> 
There has been an error processing your request

Exception printing is disabled by default for security reasons.

Error log record number: 931f1434a5cc0e1dd97e9415b671b30de9abc418953c16196e7f6c0d64267f22


`http://ignition.htb/catalogsearch/result/?q=cat`

# Gobuster
`gobuster dir -u http://ignition.htb -w /usr/share/dirb/wordlists/common.txt -x php,html`

Found /admin dir

# Admin login
Burp suite the top 100 common password
found password to be `qwerty123`
logged in to admin
get flag
