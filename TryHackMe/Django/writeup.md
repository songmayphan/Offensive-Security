# Meta data

Date accessed: 08/21/2021


# CTF

URL `http://10.10.148.224:8000/admin/auth/user/`

SSH Credentials found: `StrangeFox:WildNature` - after cracking hash


```

django-admin@py:~/messagebox$ su StrangeFox
Password: 
StrangeFox@py:/home/django-admin/messagebox$ whoami
StrangeFox
StrangeFox@py:/home/django-admin/messagebox$ ls
db.sqlite3  lmessages  manage.py  messagebox
StrangeFox@py:/home/django-admin/messagebox$ cd
StrangeFox@py:~$ ls
user.txt
StrangeFox@py:~$ cat user.txt 
THM{SSH_gUy_101}
StrangeFox@py:~$ 


```

```
StrangeFox@py:/home/django-admin/messagebox/messagebox$ cat home.html 
{% extends 'base.html' %}
{% block title %}Home page{% endblock %}

{% block content %}
	<body bgcolor="#E6E6FA">
	<h1><center>Message box v1.1</center></h1>
	<br>
	<center><p>Hi! Welcome back to your inbox. Seems like you got a new message!</p></center>
	<center><p>Check it out here:</p></center>
	<center><p><a href="/messages">Messages</a></p></center>
	<!-- Flag 3: THM{django_w1zzard} -->

{% endblock %}



```

